import os
from shared_modules import read_config
from prepare_data import *
from main import tomography
from make_graphs import * # for plotting
# import GUI_solution
import GUI

### Configure the paths and load the inputs dictionary 
cfg_file = "tomography"
program_path = os.path.abspath(__file__)
program_path = program_path[:program_path.rfind('/')]+'/'        
# local_path = os.path.expanduser('~/tomography/')
local_path = "/shares/users/work/osam/tomography/"
inputs = read_config(local_path+cfg_file+".cfg")
 
inputs['program_path']= program_path
inputs['local_path']  = local_path
# inputs['output_path'] = os.path.expanduser(os.path.expandvars(inputs['output_path']))+'/'
inputs['output_path'] = '/shares/users/work/osam/tomography/Output'
# inputs['tmp_folder']  = os.path.expanduser(os.path.expandvars(inputs['tmp_folder']))+'/'
inputs['tmp_folder']  = '/shares/users/work/osam/tomography/tmp'

config.wrong_dets_pref = inputs['wrong_dets']
if 'usecache' in inputs:
    config.useCache = inputs['usecache']


### Manual data entry
# inputs['shot'] = int(30583)
# inputs['shot'] = int(30530)
inputs['shot'] = int(32161)
# inputs['shot'] = int(40488)
# inputs['shot'] = int(41417)
# inputs['tmin'] = 3.2008
# inputs['tmax'] = 3.2015  
inputs['tmin'] = 1.0
inputs['tmax'] = 1.1  
inputs['tok_index'] = 9 # 9: ASDEX SXR, 10: ASDEX SXRfast; 11: ASDEX BOLO; 12: ASDEX AXUV
inputs['plot_all'] = False # separated plot for each time step
inputs['gnuplot'] = False # plot with gnuplot (faster than in matplotlib)
inputs['plot_contours'] = True # check contour plot



### Load and prepare the tokamak data (geometry, SXR configs)
tok = loaddata(inputs)
# tok.prepare_tokamak()
# import pdb; pdb.set_trace()

# GUI_solution.main(inputs, tok)

### Initiate tomography calculations
# inputs, tokamak, progress,output = tomography(inputs,tok)

### Plotting 
# make_graphs((inputs, tokamak, progress, output), plot_svd=False)
# plots are saved in ~/tomography/tmp/
