import pickle
import numpy as np
# from make_ECEgraphs import make_ECEgraphs # functions to plot with multiprocessing

from multiprocessing import Process, Pool, cpu_count
# import threading
# threading._DummyThread._Thread__stop = lambda x:40

from make_graphs import matplotlib_image    

with open('./data_for_testing/args.pickle', 'rb') as handle:
    args = pickle.load(handle)


for a in args:
    matplotlib_image(a)

"""
try:
    n_cpu = cpu_count()
    p = Pool(n_cpu)
except:
    n_cpu = 1

p.imap(matplotlib_image,args,1 )

p.close()
p.join()
"""
# try:
    # p.close()
    # p.join()
    
# print(outputECE["path_tmp"])
# print(outputECE.keys())

# make_ECEgraphs(outputECE)

