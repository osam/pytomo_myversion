#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys, traceback
import numpy as np
import time
import config
from collections import OrderedDict
from scipy.interpolate import interp1d
from scipy.stats.mstats import mquantiles
import threading
from shutil import copyfile
import aug_sfutils as sf

from tokamak import Tokamak
from shared_modules import in1d,fast_svd,read_config, warning


global loader


class ASDEX(Tokamak):
    """
    Representation of class of tokamak. Contain basic information about geometry, borde, magnetic field and basic setting used for recontruction and postprocessing.

    ASDEX(input_path, 'SXR-slow',  shot, nx, ny,virt_chord )


    :var str name:  Name of tokamak
    :var int index: Index of the tokamak
    :var double xmin, xmax, ymin, ymax: boundaries of recontructed area of tokamak emissivity
    :var str geometry_path: Path to saved geometry, boundary or data
    :var double sigma:  Expected noise in data  += ``sigma``* sqrt(data) * sqrt(max(data))  (Poisson like noise)
    :var double min_error: Expected noise in data  += ``min_error`` * sqrt(max(data))
    :var int shot:  NUmber of selected shot
    :var int norm: Used to normalize input parameters to meters
    :var str t_name:  Physical units used to measure time for the tokamak
    :var int npix:  total number of pixels (nx*ny)
    :var double dx: Width of one cell
    :var double dy: Height of one cell
    :var bool local_network: True if it is possible to load data from network

    :var array data: Input signals from main diagnostics
    :var array tvec: Time vector that belongs to the ``data``
    :var array tsteps: Lenght of tvec /  data
    :var array error: Expected errors of data,
    :var array dets: Array of all detectors in diagnostics
    :var array wrong_dets: Array of deleted detectors in diagnostics
    #:var array magx, magy: Arrays of magnetic lines possitions
    :var spmatrix Tmat: Matrix of geometry
    :var array Xchords, Ychords: Arrays of chords coordinates, used in ``make_plots``
    :var array boundary: Coordinates of points on boundary of tokamak
    :var array emiss_0: First guess of plasma emissivity, improve convergence
    """


    def __init__(self, input_diagn, input_parameters, only_prepare=False, load_data_only=False ):

        """ Initialize new tokamak and set default values.

            :param dict input_parameters:   List of selected paramerers
            :param str input_diagn:  Name of main diagnostics
            :param array vmax: artificial shift of magnetic field

        """

        name  = 'ASDEX'

        shot = input_parameters['shot']
        self.mag_diag = input_parameters['mag_diag']
        self.mag_exp  = input_parameters['mag_exp']
        self.mag_ed   = input_parameters['mag_ed']

        self.local_path   = input_parameters['local_path']
        self.program_path = input_parameters['program_path']

        path = 'geometry/'+name
        if not os.path.exists(self.local_path+path): os.makedirs(self.local_path+path)

        path += '/'+ input_diagn
        if not os.path.exists(self.local_path+path): os.makedirs(self.local_path+path)

        if input_diagn in ['SXR', 'SXR_fast'] :
            coord = [1.05, 2.2, -1.02, 0.94]  
            rad_profile = 'Emiss'

            if input_diagn == 'SXR_fast':
                path = 'geometry/'+name+'/'+ 'SXR'

        elif input_diagn in ['BOLO', 'AXUV']:               
            coord= [1, 2.25, -1.3, 1.25]   
            rad_profile = 'Emiss'

        self.geometry_path_program = self.program_path+path
        path = self.local_path+path
        
        if self.geometry_path_program+'/border.txt' != path+'/border.txt':
            copyfile(self.geometry_path_program+'/border.txt',path+'/border.txt' )

        self.input_diagn = input_diagn
        
        self.min_tvec = 0
        self.max_tvec = 9

        sigma = 0.02              
        min_error = 0.01           
        norm = 1.0           
        t_name = 's'              
        
        Tokamak.__init__(self, input_parameters, input_diagn,
                        coord,  name, norm, sigma, min_error, path, t_name)

        self.Tokamak = Tokamak
        self.allow_negative = False
        self.dets_index = (1, ) #default
        Tokamak.rad_profile = rad_profile
        self.rad_profile = rad_profile

        #share "common wisdom", 
        calib_path = path+'/calibration/'
        if not os.path.exists(calib_path):  
            try:
                os.mkdir(calib_path)
            except:
                print('error:os.mkdir(calib_path)')
                raise
        wch_path = path+'/wrong_channels/'
        if not os.path.exists(wch_path): 
            try:
                os.mkdir(wch_path)
            except:
                print('error: os.mkdir(wch_path)')
                raise

        calib_path_orig = self.geometry_path_program+'/calibration/%d.txt'%self.shot
        if (not os.path.isfile(calib_path+'%d.txt'%self.shot) or not config.useCache) and os.path.isfile(calib_path_orig):
            if calib_path_orig!= calib_path+'%d.txt'%self.shot:
                copyfile(calib_path_orig, calib_path+'%d.txt'%self.shot)
            
        wch_path_orig = self.geometry_path_program+'/wrong_channels/%d'%self.shot
        if (not os.path.isfile(wch_path+'%d'%self.shot) or not config.useCache) and os.path.isfile(wch_path_orig):
            if wch_path_orig!= wch_path+'%d'%self.shot:
                copyfile(wch_path_orig, wch_path+'%d'%self.shot)

        if os.path.exists(self.geometry_path_program+'/virt_chord_profile.txt'):
            if self.geometry_path_program+'/virt_chord_profile.txt'!= path+'/virt_chord_profile.txt':
                copyfile(self.geometry_path_program+'/virt_chord_profile.txt', path+'/virt_chord_profile.txt')

        if not load_data_only:
     
            self.load_mag_equilibrium()
            
            self.min_tvec = 0
            self.max_tvec = self.tsurf[-1]+0.1
 
            self.VesselStructures()

            self.load_LBO()
            self.load_ICRH()
            
            if self.input_diagn in ('SXR', 'SXR_fast'):
                self.load_others()
  
        if input_diagn in ('SXR', 'SXR_fast') :
            if self.shot < 20807 and self.shot > 1000 :
                raise Exception('Old SXR system is not suppoted!!')
            
            self.SXR()
            self.index = 9 if input_diagn == 'SXR' else 10 
 
        if input_diagn == 'BOLO':
            self.BOLO()
            self.index = 11

        if input_diagn == "AXUV":
            self.AXUV()
            self.index = 12 

        self.max_tvec = min(self.max_tvec, self.tvec[-1])
        try:
            self.load_geom()
        except:
            print('load_geom failured')

        Tokamak._post_init(self)

        if not only_prepare:
            self.prepare_tokamak()


    def SXR(self):
        
        self.default_calb = 'variable'   
        self.allow_self_calibration = False
        self.sigma = 0.006 #just guess!!
        self.min_error = 0.004#just guess!!

        self.shift = 13
        self.boundary_lcfs = True

        from .SXR.get_sxr import loader_SXR
        global loader

        fast_data = self.input_diagn == 'SXR_fast'
        loader = loader_SXR(self.shot, self.geometry_path, fast_data,
                            experiment='AUGD', edition=0)

        self.detectors_dict= loader.detectors_dict
        self.calb_0 = loader.calb_0
        self.nl =  loader.nl
 
        self.dets_index = loader.dets_index
        self.dets = loader.dets
        self.tvec = loader.tvec
        self.Phi  = loader.Phi
        self.SampFreq = loader.SampFreq

        if self.input_diagn == 'SXR_fast':
            self.sample_freq = 5e5
        else:
            self.sample_freq = 5e3

        
    def BOLO(self):
        
        self.allow_self_calibration = True
        self.use_median_filter = False
        self.beta = 0
        self.shift = 5
        self.boundary_lcfs = False


        from .BOLO.get_bolo import loader_BOLO
        
        global loader

        loader = loader_BOLO(self.shot, self.geometry_path, experiment='AUGD', edition=0)
                
        self.detectors_dict = loader.detectors_dict
   
        self.calb_0 = loader.calb_0
        self.nl =  loader.nl
        self.tvec = loader.tvec

        self.dets_index = loader.dets_index
        self.dets =  loader.dets
        self.sample_freq = 1/loader.dt

        self.total_rad = loader.get_total_rad()


    def AXUV(self):
        
        self.allow_self_calibration = False
        self.boundary_lcfs = False
        self.shift = 16
        self.sigma = 0.01
        self.min_error = 0.01
        
        
        from .AXUV.get_axuv import loader_AXUV
        global loader

        loader = loader_AXUV(self.shot, self.geometry_path, experiment='AUGD', edition=0)
                  
        self.detectors_dict = loader.detectors_dict
        self.calb_0 = loader.calb_0
        self.nl = loader.nl
        self.dets_index = loader.dets_index
        self.dets = loader.dets
        self.tvec = loader.tvec
        self.tvec = np.linspace(loader.tvec[0], loader.tvec[-1], int(loader.tvec.size/loader.n_smooth))
        self.sample_freq =  500e3/loader.n_smooth

        
    def load_geom(self):
        global loader

        try:
            loader.load_geom(self.geometry_path_program)
        except Exception as e:
            print('Geometry could not be loaded:: ')
            
        if hasattr(loader, 'geometry_version'):
            self.geometry_version =  loader.geometry_version


    def get_data(self, failsave=True, tmin=-np.infty, tmax=np.infty):
        
        global loader

        t = time.time()

        tvec, data, error = loader.get_data(tmin, tmax)
        wrong_dets_damaged = np.where(in1d(loader.all_los, loader.wrong_dets_damaged))[0]

        self.wrong_dets_damaged  = np.unique(np.r_[self.wrong_dets_damaged, wrong_dets_damaged])
    
        ind_t = slice(None, None)
        if  len(tvec) > 5000:  ind_t = np.random.randint(len(tvec), size=1000)

        mean_data = np.nanmean(data[ind_t], axis=0)

#include also "nonstatistical noise"  by min_error and sigma
        error =  np.hypot(error, np.single(mean_data*self.sigma), out=error)
        error += np.abs(mean_data)*self.min_error

        print('loading time: %.1f s'%( time.time()-t))

        return  tvec, data, error


    def load_LBO(self):
        #print 'load_LBO'
        lbo_path = self.local_path +"/geometry/ASDEX/LBO/"
        
        if not os.path.exists(lbo_path):
            try:
                os.mkdir(lbo_path)
            except:
                print('error: os.mkdir(lbo_path)')
                
        lbo_path += "LBO_"+str(self.shot)+".txt"

        try:
            self.impur_inject_t = np.atleast_1d(loadtxt(lbo_path))             
            if all(np.isnan(self.impur_inject_t)):
                self.impur_inject_t = None
        except Exception as e:
            lbo = sf.SFREAD('LBO', self.shot)
            if lbo.status:
                    
                #20Hz synchronization with the time 0
                LBOtvec = lbo('Time_DAC')
                LBO = lbo('DAC-Sig').T
 
                impur_inject_t = np.linspace(0, 8, 8*20+1)
                LBO_ind = np.zeros_like(impur_inject_t, dtype='bool')
                for i in np.where(np.any(LBO>1, axis=0))[0]:  
                    LBO_ind[(impur_inject_t>=LBOtvec[i]+1e-3)&(impur_inject_t<=LBOtvec[i+1]-1e-3)]=True

                lbo_times = impur_inject_t[LBO_ind]
                np.savetxt(lbo_path, lbo_times, fmt='%3.5f' )
                self.impur_inject_t = lbo_times
            else:
                self.impur_inject_t = None
                np.savetxt(lbo_path, (np.nan, ))


    def load_others(self):
        #load centrifugal asymmetry profile and bremsstrahlung level 

        data_path = self.geometry_path+'/Bremsstrahlung_%d.npz'%self.shot
        
        if os.path.isfile(data_path):
            return

        diag = 'EQH' if self.mag_diag == 'TRA' else self.mag_diag

        if not hasattr(self, 'equ'):
            stat = False
        else:
            stat = self.equ.sf.status
        if not stat:
            self.equ = sf.EQU(self.shot, diag=diag, exp=self.mag_exp, ed=self.mag_ed)
        if not self.equ.sf.status:
            print('Warning: equlibrium for shot:%d diag:%s  exp:%s  ed:%d  was not found!! FPP will be used'%(self.shot, diag, self.mag_exp, self.mag_ed))
            self.mag_diag = 'FPP'
            self.mag_exp = 'AUGD'
            self.mag_ed = 0
            self.equ = sf.EQU(self.shot, diag=self.mag_diag, exp=self.mag_exp, ed=self.mag_ed)
            if not self.equ.sf.status:
                raise Exception('equlibrium for shot:%d diag:%s  exp:%s  ed:%d  was not found!! use another one'%(self.shot, self.mag_diag, self.mag_exp, self.mag_ed))
    
        ida = sf.SFREAD('IDA', self.shot)
        vta = sf.SFREAD('VTA', self.shot)

        if ida.status:
            rhop = ida('rhop')
            tvec = ida('time')
            ne   = ida('ne')
            Te   = ida('Te')
            
            ne_low  = ida('ne_lo')
            Te_low  = ida('Te_lo')
            ne_high = ida('ne_up')
            Te_high = ida('Te_up')

            ind_t = slice(*tvec.searchsorted((self.min_tvec, self.max_tvec)))
            rhop = rhop[ind_t].mean(0)
            ind_r = slice(0, rhop.searchsorted(1), 3)
            ne = ne[ind_r, ind_t]
            Te = Te[ind_r, ind_t]
            ne_low = ne_low[ind_r, ind_t]
            Te_low = Te_low[ind_r, ind_t]
            ne_high = ne_high[ind_r, ind_t]
            Te_high = Te_high[ind_r, ind_t]
            rhop = rhop[ind_r]
            tvec = tvec[ind_t]

        elif vta.status:

            tvec = vta('TIM_CORE')
            ind_t = slice(*tvec.searchsorted((self.min_tvec, self.max_tvec)))

            tvec = tvec[ind_t]

            R = vta.getobject('R_core', nbeg=ind_t.start+1, nend=ind_t.stop)
            z = vta.getobject('Z_core')
            z, R = meshgrid(z, R)
            rho = sf.rz2rho(self.equ, R, z, tvec)

            Te_     = vta.getobject('Te_c'   , nbeg=ind_t.start+1, nend=ind_t.stop)
            Ne_     = vta.getobject('Ne_c'   , nbeg=ind_t.start+1, nend=ind_t.stop)
            Ne_up_  = vta.getobject('Neupp_c', nbeg=ind_t.start+1, nend=ind_t.stop)
            Ne_low_ = vta.getobject('Nelow_c', nbeg=ind_t.start+1, nend=ind_t.stop)
            Te_up_  = vta.getobject('Teupp_c', nbeg=ind_t.start+1, nend=ind_t.stop)
            Te_low_ = vta.getobject('Telow_c', nbeg=ind_t.start+1, nend=ind_t.stop)

            rhop = np.linspace(0, 1, 100)

            valid = (Te_ > 10) & (Ne_ > 10)
            valid_ind = np.any(valid, axis=1)
            tvec = tvec[valid_ind]

            Ne_low_[Ne_low_ <= 0] = np.nan
            Te_low_[Te_low_ <= 0] = np.nan

            Te      = np.zeros((len(rhop), sum(valid_ind)))
            ne      = np.zeros_like(Te)
            ne_high = np.zeros_like(Te)
            ne_low  = np.zeros_like(Te)
            Te_high = np.zeros_like(Te)
            Te_low  = np.zeros_like(Te)

            for i, ii in enumerate(np.where(valid_ind)[0]):
                ind = valid[ii]
                sind = np.arange(rho.shape[1])[ind][argsort(rho[ii, ind])]
                Te[:, i] = np.interp(rhop,  rho[ii, sind], Te_[ii, sind], left=np.nan)
                ne[:, i] = np.interp(rhop,  rho[ii, sind], Ne_[ii, sind], left=np.nan)
                ne_high[:, i] = np.interp(rhop, rho[ii, sind], Ne_up_ [ii, sind], left=np.nan)
                ne_low [:, i] = np.interp(rhop, rho[ii, sind], Ne_low_[ii, sind], left=np.nan)
                Te_high[:, i] = np.interp(rhop, rho[ii, sind], Te_up_ [ii, sind], left=np.nan)
                Te_low [:, i] = np.interp(rhop, rho[ii, sind], Te_low_[ii, sind], left=np.nan)   
        else:
            np.savez(data_path)
            return 

        jou = sf.SFREAD('JOU', self.shot)
#        He = ''.join([g.decode('utf-8') for g in  jou('FILLING')['GAS_He']])
        He = ''.join(jou('FILLING')['GAS_He'].astype(str))
  
        Zeff = 1.1  #BUG just guess!!
        if He == 'He':  Zeff = 2.1

        from .radiation.Bremsstrahlung import CalcBackgroundRadiation, CalcRadiation, CalcZ

        BR     = np.ones_like(Te)*np.nan
        BR_low = np.ones_like(Te)*np.nan
        BR_up  = np.ones_like(Te)*np.nan
        W      = np.ones_like(Te)*np.nan
        W_low  = np.ones_like(Te)*np.nan
        W_up   = np.ones_like(Te)*np.nan
        
        ind = np.isfinite(Te) & np.isfinite(ne)
        BR[ind] = CalcBackgroundRadiation(ne[ind], Te[ind], Zeff)
        W[ind]  = CalcRadiation(ne[ind], Te[ind], 1e-4)
        ind = np.isfinite(Te_low) & np.isfinite(ne_low)
        BR_low[ind] = CalcBackgroundRadiation(ne_low[ind], Te_low[ind], Zeff/1.1)
        W_low[ind]  = CalcRadiation(ne_low[ind], Te_low[ind], 1e-4)
        ind = np.isfinite(Te_high) & np.isfinite(ne_high)
        BR_up[ind] = CalcBackgroundRadiation(ne_high[ind], Te_high[ind], Zeff*1.3)
        W_up[ind]  = CalcRadiation(ne_high[ind], Te_high[ind], 1e-4)

        from scipy.constants import e, m_p, m_u, mu_0

        m_i = Zeff*2
        m_z = 183.
        Z_i = 1.

        cez = sf.SFREAD('CEZ', self.shot)
        coz = sf.SFREAD('COZ', self.shot)
        if cez.status:
            cx = cez
        elif coz.status:
            cx = coz
        else:
            cx = None

        if cx is not None:
            if 'R_time' in cx.sf.objects:
                R_cxrs = cx('R_time')
                z_cxrs = cx('z_time')
                ind = np.all(R_cxrs > 0,0)
            else:
                R_cxrs = cx('R')
                z_cxrs = cx('z')
                ind = R_cxrs > 0
       
            tvec_cxrs = cx('time')

            R_cxrs = R_cxrs[..., ind]
            z_cxrs = z_cxrs[..., ind]
            Ti   = cx('Ti_c').T[:, ind]+1
            vtor = cx('vrot').T[:, ind]

            R0 = np.interp(tvec_cxrs, self.equ.time, self.equ.Rmag)[:,None]

            rho_asym = sf.rz2rho(self.equ, R_cxrs, z_cxrs, tvec_cxrs)
       
            mach = sqrt(2*m_u/e*vtor**2/(2*Ti))
      
            Te_ = interp1d(tvec, Te, axis=1, bounds_error=False)(tvec_cxrs)
            Te_ = np.array([np.interp(r, rhop, te) for r, te in zip(rho_asym, Te_.T)])

            Te_[~np.isfinite(Te_)] = Ti[~np.isfinite(Te_)]
            Z_w  = CalcZ(Te_)

            M02 = (mach**2*m_z/m_i*(1-(m_i/m_z*Z_w*Zeff)*Te_/(Ti+Zeff*Te_)))/R_cxrs**2*R0**2

            asym_CF = exp((M02/R0**2)*(R0**2-R_cxrs**2))
            asym_CF = abs(1-asym_CF)/(asym_CF+1) #not consistent with tomography definition for large Mach! 

            asym = np.array([np.interp(rhop, np.r_[0, sort(r)], np.r_[0, a[argsort(r)]], right=0) for r, a in zip(rho_asym, asym_CF)])
                    
            asym = interp1d( tvec_cxrs, asym, axis=0, bounds_error=False)(tvec).T
           
        else:
            asym = np.zeros_like(BR )
                    
        np.savez_compressed(data_path,
                    tvec=np.single(tvec), rho=np.single(rhop),
                    BR=np.single(BR.T),
                    BR_low=np.single(BR_low.T),
                    BR_high=np.single(BR_up.T),
                    W=np.single(W.T),
                    W_low=np.single(W_low.T),
                    W_high=np.single(W_up.T),
                    asym=np.single(asym.T))


    def load_ICRH(self):
        
        icrh_path = self.local_path +"/geometry/ASDEX/ICRH_data/"
        if not os.path.exists(icrh_path):
            try:
                os.mkdir(icrh_path)
            except:
                print('error: os.mkdir(icrh_path)')
                raise
        ICRH_pos_file = icrh_path+"/ICRH_pos_"+str(self.shot)+".npz"

        try:
            assert config.useCache, 'do not use cache'
            ICRH_resonance = np.load(ICRH_pos_file, allow_pickle=True, encoding='latin1')
            if 'tvec' in ICRH_resonance:
                self.ICRH_resonance = {k:v for k, v in ICRH_resonance.items()}
        except:
            print('loading ICRH')

            try:
                icp = sf.SFREAD('ICP', self.shot)
                assert icp.status, 'No ICP shotfile'

                freqs = np.array([icp('Frequenz')['Freq%s'%d] for d in range(1, 5)])
                ICRH_tvec = icp('T-B')
                
                PICRF = c_[[icp.getobject( "pnet%s"%d) for d in range(1, 5)]]
                ICRH_on = PICRF > 1e5
                
                if self.shot > 30160:
                    mbi = sf.SFREAD('MBI', self.shot)
                    mag_tvec = mbi.gettimebase('BTFABB')
                    Bt = mbi('BTFABB')
                else:
                    mai = sf.SFREAD('MAI', self.shot)
                    mag_tvec = mai.gettimebase('BTF')
                    Bt = mai.getobject('BTF', cal=True)

                Bt = np.interp(ICRH_tvec, mag_tvec[:len(Bt)-1], Bt[:-1])

                from scipy import constants
                R0 = 1.65
                BtR = abs(Bt)*R0 #T*m
                #He3 minority discharges!!!
                m_q_minor = 3/2. if self.shot in np.r_[28313:28321, 28390, 28391, 31552:31556, 31562:31564, 31585, 31587, 31566, 34694:34705 ] else 1
                Bc = (2*np.pi*freqs[None, :]*m_q_minor*constants.m_u/constants.e)
                R = BtR[:, None]/Bc

                R[~ICRH_on.T] = np.nan
                self.ICRH_resonance = {'tvec':np.single(ICRH_tvec), 'R': np.single(R)}

                np.savez_compressed(ICRH_pos_file, **self.ICRH_resonance)
            except Exception as e:
                np.savez_compressed(ICRH_pos_file, None)
                print('no ICRH', e)

   
    def load_mag_equilibrium(self):

        print('\n\nASDEX:load_mag_equilibrium\n\n')

        eq_path = self.local_path + '/geometry/ASDEX/equilibrium/'

        if not os.path.exists(eq_path):
            try:
                os.mkdir(eq_path)
            except:
                print('error: os.mkdir(eq_path)')
                raise
        eq_path += 'MagField_fast_%d.npz'%self.shot

        try:
            data = np.load(eq_path, allow_pickle=True, encoding='latin1')

            self.tsurf      = data['tsurf']
            self.surf_coeff = data['surf_coeff']
            mag_diag        = data['diag'].item()
            if isinstance(mag_diag, bytes): mag_diag = mag_diag.decode('utf-8')
            if mag_diag != self.mag_diag and not (self.mag_diag == 'EQI' and mag_diag == 'EQH'):
                warning('Warning: requested equilibrium differs from the stored: %s vs %s'%(mag_diag, self.mag_diag))

            self.mag_axis = {'tvec':data['tvec_fast'], 'Rmag':data['Rmag'], \
                'Zmag':data['Zmag'], 'ahor':data['ahor'], 'bver':data['bver']}

        except Exception as e:
            print('Magnetic equilibrium could not be loaded: '+str(e))
            
            diag = 'EQH' if self.mag_diag == 'TRA' else self.mag_diag 
            eq_diags = ((diag, self.mag_exp, self.mag_ed), ('EQI', 'AUGD', 0), ('FPP', 'AUGD', 0))

            for diag, mag_exp, mag_ed in eq_diags:
                self.equ = sf.EQU(self.shot, diag=diag, exp=mag_exp, ed=mag_ed)
                if self.equ.sf.status: break
                warning('Warning: equlibrium for shot:%d diag:%s  exp:%s  ed:%d  was not found!! other will be used'%(self.shot, diag, mag_exp, mag_ed))

            if not self.equ.sf.status:
                raise Exception('equlibrium for shot:%d diag:%s  exp:%s  ed:%d  was not found!! use another one'%(self.shot, self.mag_diag, self.mag_exp, self.mag_ed))

            mag_diag = self.mag_diag
            from .mag_equ import  Equlibrium

            EQU = Equlibrium(self.equ, self.shot, mag_diag, self.mag_exp, self.mag_ed)
            
            if mag_diag == 'TRA':
                output = EQU.getTranspEquilibrium()
            else:
                output = EQU.getStandartEquilibrium()
            output[mag_diag] = mag_diag
            np.savez_compressed(eq_path, diag=mag_diag, exp=self.mag_exp, ed=self.mag_ed, **output)

            self.tsurf      = output['tsurf']
            self.surf_coeff = output['surf_coeff']
            self.mag_axis   = output
            self.mag_axis['tvec'] = output['tvec_fast']

        self.mag_dt = np.amax(np.diff(self.mag_axis['tvec']))

        if mag_diag == 'TRA':
            self.mag_axis['Rmag'] = np.copy(self.surf_coeff[:, -1, 0, 0])
            self.mag_axis['Zmag'] = np.copy(self.surf_coeff[:, -1, 0, 1])
            self.surf_coeff[:, -1, 0, :2] = 0

       
    def mag_equilibrium(self, tvec, preferCache=True, dryRun=False, return_mean=False,
                  surf_slice=slice(None, None), n_rho=60, n_theta=100, rho=None, radial_coordinate=None):
 
        """ Method to load magnetic field (contours). Try to load from local cache, if it is not possible try 
        to load from JET network and store it localy. Loaded field is only with limited precision (max 100 timeslices and it is interpolated)
         
        :param array tvec: Time vector  of requested  magnetic field
        :param bool dryRun: Do not return interpolated field
        :param bool preferCache: Use cached data if possible, it is faster

        :var array magx, magy: Arrays of magnetic lines possitions
        :var array tsurf: Time vector that belongs to ``magx``, ``magy``
        :raises Asseration: No connection to JET network"
        :rtype:  Coordinates of magnetic field interpolated to the requested time vector ``tvec``
        """
              
        def surf_polyval(rho, theta,  p_rcos, p_zcos, p_rsin, p_zsin):
            #evaluate equilibrium polynom
            nmom = np.size(p_rcos, -1)
            rho = np.atleast_1d(rho)

            P = np.double(np.dstack((p_rcos, p_zcos, p_rsin, p_zsin)))
            moments = np.zeros((nmom, 4, np.size(rho)))
                
            #horner scheme
            for p in P:
                moments *= rho[None, None]
                moments += p[:, :, None]

            angle = np.outer(np.arange(nmom), theta )
            C = np.cos(angle)
            S = np.sin(angle)
            r_plot = np.tensordot(moments[:, 0].T, C, axes=([-1, 0])) #rcos
            r_plot+= np.tensordot(moments[:, 2].T, S, axes=([-1, 0])) #rsin
            z_plot = np.tensordot(moments[:, 1].T, C, axes=([-1, 0])) #zcos
            z_plot+= np.tensordot(moments[:, 3].T, S, axes=([-1, 0])) #zsin

            return r_plot.T, z_plot.T

        if rho is None:
            rho = np.linspace(0, 1, n_rho)#[surf_slice]
        tvec = np.atleast_1d(tvec).astype('double')      
        tvec  =  np.copy(tvec)
        tvec[tvec<self.tsurf[0]] = self.tsurf[0]
        tvec[tvec>self.tsurf[-1]] = self.tsurf[-1]
        
        ind  = slice(max(0, np.searchsorted(self.tsurf, tvec.min()-self.mag_dt*2)-1), 
                     min(len(self.tsurf), np.searchsorted( self.tsurf, tvec.max()+self.mag_dt*2)+1))

        from shared_modules import MovingAveradge

        theta = np.linspace(-np.pi, np.pi, n_theta)
        fast_tvec = self.mag_axis['tvec']

        #calculate separatrix
        if len(tvec) > 1:
            n_smooth = max(int(np.mean(np.diff(tvec))/np.mean(np.diff(fast_tvec))), 1)
        else:
            n_smooth = np.sum((fast_tvec<tvec[-1]+self.mag_dt)&(fast_tvec>tvec[0]-self.mag_dt))

        surf_coeff = np.copy(self.surf_coeff[ind])
            
        tsurf = np.copy(self.tsurf[ind])

        surf_coeff = interp1d(tsurf, surf_coeff, axis=0, copy=False, bounds_error=False, fill_value=np.nan)(tvec)

        if return_mean:
            ind_fast = slice(fast_tvec.searchsorted(tvec[0]-self.mag_dt),
                             fast_tvec.searchsorted(tvec[-1]+self.mag_dt)+1)
            ahor = np.atleast_1d(np.median(self.mag_axis['ahor'][ind_fast]))
            bver = np.atleast_1d(np.median(self.mag_axis['bver'][ind_fast]))
            R0   = np.atleast_1d(np.median(self.mag_axis['Rmag'][ind_fast]))
            Z0   = np.atleast_1d(np.median(self.mag_axis['Zmag'][ind_fast]))
            
        else:
            fast_tvec = fast_tvec

            Dt = (tvec[-1]-tvec[0])/len(tvec)+(fast_tvec[-1]-fast_tvec[0])/fast_tvec.size
            tmin = max(tvec[0] , fast_tvec[0 ]) - Dt*(n_smooth+1)
            tmax = min(tvec[-1], fast_tvec[-1]) + Dt*(n_smooth+1)
            imin = len(fast_tvec)-(-fast_tvec[::-1]).searchsorted(-tmin)
            imax = fast_tvec.searchsorted(tmax)

            ahor = MovingAveradge(np.double(np.copy(self.mag_axis['ahor'][imin:imax])), n_smooth)
            bver = MovingAveradge(np.double(np.copy(self.mag_axis['bver'][imin:imax])), n_smooth)
            R0   = MovingAveradge(np.double(np.copy(self.mag_axis['Rmag'][imin:imax])), n_smooth)
            Z0   = MovingAveradge(np.double(np.copy(self.mag_axis['Zmag'][imin:imax])), n_smooth)
            mag_tvec = fast_tvec[imin:imax]

            ahor = np.interp(tvec, mag_tvec, ahor)
            bver = np.interp(tvec, mag_tvec, bver)
            R0   = np.interp(tvec, mag_tvec,   R0)
            Z0   = np.interp(tvec, mag_tvec,   Z0)

        #renormalize almost circular  flux surfaces by fast equilibrium data
        surf_coeff[..., 0::2] *= ahor[:, None, None, None]
        surf_coeff[..., 1::2] *= bver[:, None, None, None]
        surf_coeff[:, -1, 0, 0] += R0
        surf_coeff[:, -1, 0, 1] += Z0
             
        #apply shift from the config file
        input_parameters = read_config(self.local_path+'/tomography.cfg')
        self.magfield_shift_core = input_parameters['magfield_shift_core']
        self.magfield_shift_lcfs = input_parameters['magfield_shift_lcfs']

        #shift only the center, not the separatrix            
        surf_coeff[:, -1, 0, :2] += self.magfield_shift_core
        surf_coeff[:, -2, 0, :2] -= self.magfield_shift_core
        surf_coeff[:, -2, 0, :2] += self.magfield_shift_lcfs
        
        if  return_mean:      
            p_rcos, p_zcos, p_rsin, p_zsin = np.mean(surf_coeff, 0).T
            magx, magy = surf_polyval(rho, theta, p_rcos.T, p_zcos.T, p_rsin.T, p_zsin.T)        
        else:
            p_rcos, p_zcos, p_rsin, p_zsin = surf_coeff.T
            p_rcos, p_zcos, p_rsin, p_zsin = p_rcos.T, p_zcos.T, p_rsin.T, p_zsin.T            
            magx = np.empty((np.size(theta), np.size(rho), np.size(tvec)), dtype='single')
            magy = np.empty((np.size(theta), np.size(rho), np.size(tvec)), dtype='single')

            for it, t in enumerate(tvec): #slowest
                magx[:, :, it], magy[:, :, it] = surf_polyval(rho, theta, p_rcos[it], p_zcos[it], p_rsin[it], p_zsin[it])

        if np.any(np.isnan(magx)):
            raise Exception('nans in mag. surfaces!!')
        
        if radial_coordinate is None: radial_coordinate = self.radial_coordinate
        
        if radial_coordinate == 'r_a'  and np.size(rho) > 1:
            self.convert_rho_2_r_a(rho, magx, magy)
        
        if radial_coordinate == 'r_V'  and np.size(rho) > 1:
            self.convert_rho_2_r_V(rho, magx, magy)
        
        return rho, magx, magy


    def VesselStructures(self):
        try:
            from aug_sfutils import getgc
            gc_d = getgc()
            self.struct_dict = {c:np.array((gc_d[c].r, gc_d[c].z)) for c in gc_d.keys()}
        except:
            traceback.print_exc()
    
#https://www.aug.ipp.mpg.de/cgibin/local_or_sfread/cview.cgi?shot=29022
    def ShotOverview(self):
        return

        if not os.path.exists(self.local_path +'/geometry/ASDEX/overview_plots'):
            os.makedirs(self.local_path +'/geometry/ASDEX/overview_plots')
            
        name  = self.local_path +'/geometry/ASDEX/overview_plots/overview_%d.png'%self.shot

        if os.path.isfile(name):
            return 
        
        print('loading overview plot')
        url = 'https://www.aug.ipp.mpg.de/cgibin/local_or_sfread/cview.cgi?shot=%d'

        url_backup = 'https://www.aug.ipp.mpg.de/aug/local/aug_only/plotsigweb/plotsigweb.%d.png'
        u = 'dG9kc3RyY2k=\n'
        p = 'cGl0b21laGVzbG8=\n'

        import urllib.request, urllib.error, urllib.parse,base64

        try:
            password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(None, "https://www.aug.ipp.mpg.de/cgibin/local_or_sfread/", u.decode('base64'), p.decode('base64'))

            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
            opener  = urllib.request.build_opener(handler)

            timeout = 120
            try:
                imgData = opener.open(url%self.shot, timeout=timeout).read()
            except:
                imgData = opener.open(url_backup%self.shot, timeout=timeout).read()

            output = open(name,'wb')
            output.write(imgData)
            output.close()
        except Exception as e:
            print(e, 'ShotOverview plot was not loaded')
            pass
