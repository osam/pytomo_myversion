""" Module providing the means to read ASDEX Upgrade data

Example:

sf = dd.shotfile()
if sf.Open(diagnostic,nshot):
   timebase = sf.GetTimebase(signal)
   signal = sf.GetSignal(signal)
   sf.Close() 
"""

import os
import ctypes as ct
import numpy as np
# import dics
from . import dics

# rdm 13.08.12: lbuf is ct.c_uint32, no longer ct.c_int32


# ddlib = '/afs/ipp/aug/ads/lib64/@sys/libddww8.so.8.1'
# if not os.path.isfile(ddlib):
    # ddlib = '/afs/ipp/aug/ads/lib64/amd64_sles11/libddww8.so.8.1'

ddlib = '/shares/software/aug-dv/moduledata/ads/Linux-generic-x86_64/lib64/libddww8.so'
libddww = ct.cdll.LoadLibrary(ddlib)

def GetError(error):
    try:
        err = ct.c_int32(error)
    except TypeError:
        err = ct.c_int32(error.value)
    isError = libddww.xxsev_(ct.byref(err))==1
    isWarning = libddww.xxwarn_(ct.byref(err))==1
    if isError or isWarning:
        id   = ct.c_char_p(b'')
        text = ct.c_char_p(b' '*255)
        unit = ct.byref(ct.c_int32(-1))
        ctrl = ct.byref(ct.c_uint32(3))
        lid   = ct.c_uint64(0)
        ltext = ct.c_uint64(255)
        libddww.xxerrprt_(unit, text, ct.byref(err), ctrl, id, ltext, lid);
        if isError:
            print(text.value.strip())


class dd_info:
    status = False

class shotfile:
    """ Shotfile class to read ASDEX Upgrade data """

    def Open(self, diagname, nshot, experiment='AUGD', edition=0):
        """ Open a shotfile \n\n """
        if hasattr(self, '_diaref'):
            self.Close()
        diagname = diagname.encode('utf8') if not isinstance(diagname, bytes) else diagname
        experiment = experiment.encode('utf8') if not isinstance(experiment, bytes) else experiment
        error     = ct.c_int32(0)
        _error    = ct.byref(error)
        edit      = ct.c_int32(edition)
        _edition  = ct.byref(edit)
        self._diaref = ct.byref(ct.c_int32(0))
        cshot  = ct.c_uint32(0)
        shot   = ct.c_uint32(nshot)
        diag   = ct.c_char_p(diagname)
        exp    = ct.c_char_p(experiment)
        dat    = 18*b'd'
        date   = ct.c_char_p(dat)
        _cshot = ct.byref(cshot)
        _shot  = ct.byref(shot)
        lexp   = ct.c_uint64(len(experiment))
        ldiag  = ct.c_uint64(len(diagname))
        ldate  = ct.c_uint64(len(dat))

        result = libddww.ddopen_(_error, exp, diag, _shot, _edition, self._diaref, \
                                 date, lexp, ldiag, ldate)
        GetError(error)
        if result == 0:
            self.edition = edit.value
            self.units = None
            self.date = date.value
        else:
            del self._diaref
            GetError(error)
        return hasattr(self, '_diaref')

    def Close(self):
        """ Close a shotfile """
        if hasattr(self, '_diaref'):
            error   = ct.c_int32(0)
            _error  = ct.byref(error)

            result = libddww.ddclose_(_error, self._diaref)

            GetError(error)
            print('DDclose: edition %d closed ' %self.edition)
            del self._diaref

    def GetInfo(self, name, check=True):
        """ Returns information about the specified signal."""
# Assuming: not more than one TB, not more than one AB 
        if hasattr(self, '_diaref'):

            name = name.encode('utf8') if not isinstance(name, bytes) else name
            output = dd_info()
            rel = self.GetRelations(name)
            output.error  = rel.error
            output.tname   = None
            output.aname   = None
            output.tlen    = None
            output.index   = None
            output.units   = None
            output.address = None
            output.bytlen  = None
            output.level   = None
            output.status  = None
            output.ind     = None
            if rel.error == 0:
                jtime = None
                jarea = None
                for jid, id in enumerate(rel.typ):
                    if id == 8:
                        jtime = jid
                        output.tname = rel.txt[jid]
                    if id == 13:
                        jarea = jid
                        output.aname = rel.txt[jid]
                output.index = jtime

                head = self.GetObjectHeader(name)
                output.error = head.error
                if head.error == 0:
                    output.buf     = head.buffer
                    output.objtyp  = output.buf[0]
                    output.level   = output.buf[1]
                    output.status  = output.buf[2]
                    output.error   = output.buf[3]
                    output.address = output.buf[12]
                    output.bytlen  = output.buf[13]
                    output.ndim    = output.buf[16]
                    if output.objtyp in (6, 7, 8, 13):
                        output.units   = dics.unit_d[output.buf[15]]
                        output.estatus = output.buf[17]
                        output.fmt     = output.buf[14]
                        if output.objtyp in (6, 7, 13):
                            dims       = np.array(output.buf[18:22][::-1], dtype=np.int32)
                            output.ind = dims[dims > 0]

                        if output.objtyp == 8: # TB
                            output.tlen = output.buf[21] # = dims[0]
                            output.tfmt = output.buf[14]
                        else: # SIG, SGR, AB
                            if jtime != None: # There is a relation to a TimeBase
                                tlen1 = dims[jtime]
                                if output.objtyp == 7:
                                    tlen1 = dims[0]
                                thead = self.GetObjectHeader(rel.txt[jtime])
                                tbuf = thead.buffer
                                output.tlen = tbuf[21]
                                output.tfmt = tbuf[14]
# Check consistency with TB length
                                if check and (output.tlen != tlen1) and (tlen1 != -1):
                                    print(jtime, dims)
                                    print(output.tlen, tlen1)
                                    output.tlen = -1

                        if output.objtyp == 13: # AB
                            output.atlen = output.buf[21]
                            output.afmt  = output.buf[14]
                            sizes = np.array(output.buf[18:21], dtype = np.int32)
                            output.sizes = sizes[sizes > 0]
                        else:
# Beware: other than in DDAINFO2, here 'sizes' can have less than 
# 3 dims, as the 0-sized are removed. Usually (always?) it has 1 dim.
                            if jarea != None:
                                ahead = self.GetObjectHeader(rel.txt[jarea])
                                abuf = ahead.buffer
                                output.atlen = abuf[21] # #time points of AB
                                output.afmt  = abuf[14]
                                sizes = np.array(abuf[18:21], dtype = np.int32)
                                output.sizes = sizes[sizes > 0]

            return output
        return None

    def GetParameterSetInfo( self , name ):
        """ Returns information about the specified parameter set."""
        output = dd_info()
        if hasattr(self, '_diaref'):
            name = name.encode('utf8') if not isinstance(name, bytes) else name
            info  = self.GetObjectValue( name , b'items' )
            
            error    = ct.c_int32(0)
            _error   = ct.byref( error )
            par_name = ct.c_char_p(name)
            nrec     = ct.c_int32(info)
            _nrec    = ct.byref( nrec )
            rname    = ct.c_char_p(b' '*8*info)
            items    = (ct.c_uint32*info)()
            _items   = ct.byref(items)
            format   = (ct.c_uint32*info)()
            _format  = ct.byref( format )
            devsig   = (ct.c_int32*info)()
            _devsig  = ct.byref(devsig)
            lname  = ct.c_uint64( len(name) )
            lrname = ct.c_uint64( 8*info )

            result = libddww.ddprinfo_(_error, self._diaref, par_name, _nrec, rname, \
                                       _items, _format, _devsig, lname, lrname)
            output.error = error.value
            if error.value != 0:
                GetError( error )
            output.N_items = nrec.value
            output.names  = []
            output.items  = []
            output.format = []
            output.devsig = []
            for i in range(info):
                tmp = rname.value[8*i:8*(i+1)]
                if tmp.strip() != b'':
                    output.names.append(tmp)
            n_pars = len(output.names)
            for j in range(n_pars):
                output.items.append(items[j])
                output.format.append(format[j])
                output.devsig.append(devsig[j])
            
        return output

    def GetParameterInfo(self, set_name, par_name):
        """ Returns information about the parameter 'par_name' of the parameter set 'set_name'."""

        if hasattr(self, '_diaref'):
            par_name = par_name.encode('utf8') if not isinstance(par_name, bytes) else par_name
            set_name = set_name.encode('utf8') if not isinstance(set_name, bytes) else set_name
            output = dd_info()
            error   = ct.c_int32(0)
            pset    = ct.c_char_p(set_name)
            par     = ct.c_char_p(par_name)
            item    = ct.c_uint32(0)
            format  = ct.c_uint16(0)
            _error  = ct.byref(error)
            _item   = ct.byref(item)
            _format = ct.byref(format)
            lpar = ct.c_uint64(len(par_name))
            lset = ct.c_uint64(len(set_name))

            result = libddww.dd_prinfo_(_error, self._diaref, pset, par, _item, _format, lset, lpar)

            GetError(error)
            output.error = error.value
            if error.value == 0:
                output.item = item.value
                output.fmt  = format.value
            return output
        return None

    def GetParameter(self, set_name, par_name):
        """ Returns the value of the parameter 'par_name' of the parameter set 'set_name'. """
        if hasattr(self, '_diaref'):
            par_name = par_name.encode('utf8') if not isinstance(par_name, bytes) else par_name
            set_name = set_name.encode('utf8') if not isinstance(set_name, bytes) else set_name
            info = self.GetParameterInfo(set_name, par_name)
            if info.error == 0:
                error     = ct.c_int32(0)
                _error    = ct.byref(error)
                setn      = ct.c_char_p(set_name)
                lset      = ct.c_uint64(len(set_name))
                par       = ct.c_char_p(par_name)
                lpar      = ct.c_uint64(len(par_name))
                physunit  = ct.c_int32(0)
                _physunit = ct.byref(physunit)
# Characters
                if info.fmt in dics.fmt2len.keys():
                    ndim = dics.fmt2len[info.fmt]
                    nlen = ndim*info.item
                    typin  = ct.c_int32(6)
                    lbuf   = ct.c_uint32(nlen)
                    buffer = ct.c_char_p(b'd'*nlen)
                    _typin = ct.byref(typin)
                    _lbuf  = ct.byref(lbuf)
                    lndim  = ct.c_uint64(ndim)

                    result = libddww.ddparm_(_error, self._diaref, setn, par, _typin, \
                                             _lbuf, buffer, _physunit, lset, lpar, lndim)

                    GetError(error)
                    a=[]
                    for j in range(info.item):
                        a.append(buffer.value[j*ndim:(j+1)*ndim])
                    return np.array(a)
                else:
                    typin = ct.c_int32(dics.fmt2typ[info.fmt])
                    lbuf = ct.c_uint32(info.item)
                    buffer = (dics.fmt2ct[info.fmt]*info.item)()
                    _typin = ct.byref(typin)
                    _lbuf = ct.byref(lbuf)
                    _buffer = ct.byref(buffer)
                    result = libddww.ddparm_(_error, self._diaref, setn, par, _typin, \
                                             _lbuf, _buffer, _physunit, lset, lpar)
                    return np.frombuffer(buffer, dtype=np.dtype(buffer))[0]
                self.units = dics.unit_d[_physunit.value]
        return None

    def GetSignal(self, signame, cal=False, check=True):
        """Returns the specified signal group and if specified performes a conversion to the specified type (e.g. ct.c_float)."""
        if hasattr(self, '_diaref'):
            signame = signame.encode('utf8') if not isinstance(signame, bytes) else signame
            info = self.GetInfo(signame, check=check)
            if info.status == -1:
                return None
            if info.tlen == -1 and info.objtyp in (6, ):
                print('#time points inconsistent with length of TB')
                return None
            if info.error != 0:
                print('Error getting SignalGroup %s' %signame)
                return None

            if cal:
                fmt = 2
            else:
                fmt = dics.fmt2typ[info.fmt]
            leng = info.ind[0]
            if fmt == 6:
                char_len = dics.fmt2len[info.fmt]
                leng *= char_len
                buffer = self.GetArray(fmt, info.ind, clen=char_len)
            else:
                buffer = self.GetArray(fmt, info.ind)
            _buffer  = ct.byref(buffer)
            error    = ct.c_int32(0)
            _error   = ct.byref(error)
            length   = ct.c_uint32(0)
            _length  = ct.byref(length)
            k1       = ct.c_uint32(1)
            _k1      = ct.byref(k1)
            k2       = ct.c_uint32(info.ind[0])
            _k2      = ct.byref(k2)
            cfmt     = ct.c_uint32(fmt)
            _type    = ct.byref(cfmt)
            lbuf     = ct.c_uint32(leng)
            _lbuf    = ct.byref(lbuf)
            signam   = ct.c_char_p(signame)
            physdim  = 8*b'p'
            _physdim = ct.c_char_p(physdim)
            ncal     = ct.c_int32(0)
            _ncal    = ct.byref(ncal)
            lsig     = ct.c_uint64(len(signame))
            lphysdim = ct.c_uint64(len(physdim))

            if info.objtyp == 7:
# Signal
                if cal:
# Calibrated Signal
                    result = libddww.ddccsgnl_(_error, self._diaref, signam, _k1, _k2, \
                                               _type, _lbuf, _buffer, _length, _ncal, \
                                               _physdim, lsig, lphysdim)
                    GetError(error)
                    if error.value != 0:
                        if error.value == 555352323:
                            print('No calibrated data, returning uncalibrated signal')
                            return self.GetSignal(signame, cal=False)
                    else:
                        self.units = _physdim.value
                else:
                    result = libddww.ddsignal_(_error, self._diaref, signam, _k1, _k2, \
                                               _type, _lbuf, _buffer, _length, lsig)
                return np.frombuffer(buffer, dtype=np.dtype(buffer))[0]

            elif info.objtyp == 6:
# SignalGroup
                if cal:
# Calibrated SignalGroup
                    result = libddww.ddccsgrp_(_error, self._diaref, signam, _k1, _k2, \
                                               _type, _lbuf, _buffer, _length, _ncal, \
                                               _physdim, lsig, lphysdim)
                    GetError(error)
                    if error.value != 0:
                        if error.value == 556204039:
                            print('No calibrated data, returning uncalibrated signal')
                            return self.GetSignal(signame, cal=False)
                    else:
                        self.units = _physdim.value
                else:
                    result = libddww.ddsgroup_(_error, self._diaref, signam, _k1, _k2, \
                                               _type, _lbuf, _buffer, _length, lsig)
                    GetError(error)
                ind2 = []
                for el in info.ind:
                    if el != 1:
                        ind2.append(el)
                result =  np.reshape(np.frombuffer(buffer, dtype=np.dtype(buffer))[0], \
                                     newshape=ind2, order='F')
                return result
            else:
                print('The object %s is neither a SIGNAL nor a SIGNAL_GROUP' %signame)

        return None

    def GetAreabase(self, signame):
        """ Returns the areabase to the specified signal, time is always the first independent variable."""
        if hasattr(self, '_diaref'):
            signame = signame.encode('utf8') if not isinstance(signame, bytes) else signame
            info = self.GetInfo(signame)
            if info.error == 0:
                if info.objtyp not in (6, 13):
                    return None
                if len(info.sizes) > 0:
                  ind = np.append(info.sizes, info.atlen)
                  lbu = info.sizes[0]
                else:
                  ind = [info.atlen]
                  lbu = 1
                fmt = dics.fmt2typ[info.afmt]
                buffer  = self.GetArray(fmt, ind)
                _buffer = ct.byref(buffer)
                error   = ct.c_int32(0)
                _error  = ct.byref(error)
                name    = ct.c_char_p(signame)
                k1      = ct.c_int32(1)
                _k1     = ct.byref(k1)
                k2      = ct.c_int32(info.atlen)
                _k2     = ct.byref(k2)
                lbuf    = ct.c_uint32(lbu)
                _lbuf   = ct.byref(lbuf)
                cfmt    = ct.c_int32(fmt)
                _type   = ct.byref(cfmt)
                leng    = ct.c_int32(0)
                _leng   = ct.byref(leng)
                lname = ct.c_uint64(len(signame))

                result = libddww.ddagroup_(_error, self._diaref, name, _k1, _k2, _type, \
                                           _lbuf, _buffer, _leng, lname)

                GetError(error)
                ind2 = []
                for el in ind:
                    if el != 1 and el != 0:
                        ind2.append(el)
                if len(ind2) > 1:
                    abase = np.reshape(np.frombuffer(buffer, dtype = np.dtype(buffer))[0], \
                                       newshape=ind2, order='F')
                    return abase.T
                elif len(ind2) == 1:
                    return np.frombuffer(buffer, dtype = np.dtype(buffer))[0]

        return None

    def GetTimebase(self, signame, cal=False):
        """Returns the timebase corresponding to the specified signal."""
        if hasattr(self, '_diaref'):
            signame = signame.encode('utf8') if not isinstance(signame, bytes) else signame
            info = self.GetInfo(signame)
            if info.error == 0:
                if info.tlen == None:
                    print('No TimeBase found for signal %s' %signame)
                    return None
                if info.tlen == -1:
                    print('Inconsistent Timebase for signal %s' %signame)
                    return None
                if cal:
                    fmt = 2
                else:
                    fmt = dics.fmt2typ[info.tfmt]
                buffer = self.GetArray(fmt, [info.tlen])
                _buffer = ct.byref(buffer)
                error   = ct.c_int32(0)
                _error  = ct.byref(error) 
                signam  = ct.c_char_p(signame)
                k1      = ct.c_uint32(1)
                _k1     = ct.byref(k1)
                k2      = ct.c_uint32(info.tlen)
                _k2     = ct.byref(k2)
                cfmt    = ct.c_uint32(fmt)
                _type   = ct.byref(cfmt)
                lbuf    = ct.c_uint32(info.tlen)
                _lbuf   = ct.byref(lbuf)
                length  = ct.c_uint32(0)
                _length = ct.byref(length)
                lsig = ct.c_uint64(len(signame))

                result = libddww.ddtbase_(_error, self._diaref, signam, _k1, _k2, _type, \
                                          _lbuf, _buffer, _length, lsig)
                GetError(error)
                if info.tlen > 1:
                    return np.frombuffer(buffer, dtype = np.dtype(buffer))[0]
                elif info.tlen == 1:
                    return np.frombuffer(buffer, dtype = np.dtype(buffer))
                else:
                    print(b'Array size <= 0 or > 1')
                    return None
            else:
                print(b'Error:', info.error)

        return None

    def GetArray(self, fmt, ind, clen=1):
        """dd.shotfile.GetArray(fmt, ind, typin=None)\n\nCreates a ct array with the type specified in typ and the format specified in ind."""
        if ind[0] > 0:
            length = 1
            for i in ind:
                if i != 0: length *= i
            if fmt == 6:
                return ((dics.typ2ct[fmt]*clen)*length)()
            elif fmt in dics.typ2ct.keys():
                return (dics.typ2ct[fmt]*length)()
            else:
                print(b'Type %d not supported' %fmt)
        return None

    def GetObjectValue(self, name, field):
        """dd.shotfile.GetObjectValue(Name, Field)\n\nReturns the value specified in Field of the object Name."""
        if hasattr(self, '_diaref'):
            name = name.encode('utf8') if not isinstance(name, bytes) else name
            field = field.encode('utf8') if not isinstance(field, bytes) else field
            ovalue = ct.c_int32(0)
            if field in (b'relations', b'special'):
                ovalue = (ct.c_int32*8)()
            if field in (b'format', b'indices'):
                ovalue = (ct.c_int32*3)()
            if field == b'dataformat':
                ovalue = ct.c_uint16(0)
            if field == b'text':
                ovalue = (ct.c_char*64)()
            _value  = ct.byref(ovalue)
            error   = ct.c_int32(0)
            _error  = ct.byref(error)
            _name   = ct.c_char_p(name)
            _field  = ct.c_char_p(field)
            lname   = ct.c_uint64(len(name))
            lfield  = ct.c_uint64(len(field))

            result = libddww.ddobjval_(_error, self._diaref, _name, _field, _value, \
                                       lname, lfield)

            GetError(error)
            if np.size(ovalue) == 1:
                return ovalue.value
            else:
                return np.frombuffer(ovalue, dtype = np.dtype(ovalue))[0]
        return None

    def GetObjectData(self, iobj_name):
        """dd.shotfile.GetObjectData(ObjectName, fmt=ct.c_float)\n\nReturns the data part of the specified object and interpretes it as an array of the datatype specified in fmt."""
        if hasattr(self, '_diaref'):
            obj_name = iobj_name.encode('utf8') if not isinstance(iobj_name, bytes) else iobj_name
            length = self.GetObjectValue(obj_name, b'length') 
            nsize  = self.GetObjectValue(obj_name, b'size')
            fmt    = self.GetObjectValue(obj_name, b'dataformat')
            cfmt = dics.fmt2ct[fmt]

            error   = ct.c_int32(0)
            _error  = ct.byref(error)
            name    = ct.c_char_p(obj_name)
            clength = ct.c_int32(length)
            _length = ct.byref(clength)
            buffer  = (ct.c_byte*length)()
            _buffer = ct.byref(buffer)
            length  = ct.c_int32(0)
            _length = ct.byref(leng)
            lname = ct.c_uint64(len(obj_name))

            result = libddww.ddobjdata_(_error, self._diaref, name, _length, _buffer, \
                                        _length, lname)

            GetError(error)
            darr = np.frombuffer((cfmt*(length/ct.sizeof(cfmt))).from_buffer(buffer), dtype = np.dtype(cfmt))
            ny = len(darr)/nsize
            if ny == 1:
                return darr
            else:
                return darr.reshape(nsize, ny, order='F')
        return None

    def GetListInfo(self):
        if hasattr(self, '_diaref'):
            error   = ct.c_int32(0)
            _error  = ct.byref(error)
            lbuf    = ct.c_uint32(255)
            _lbuf   = ct.byref(lbuf)
            buf     = (ct.c_int32*255)()
            _buf    = ct.byref(buf)
            length  = ct.c_int32(0)
            _length = ct.byref(length) 
            result  = libddww.ddflist_(_error, self._diaref, _lbuf, _buf, _length)
            GetError(error)
            if length.value != 0:
                return np.int(buf[0:np.int(length)])
        return None

    def GetObjectHeader(self, name):
        if hasattr(self, '_diaref'):
            name = name.encode('utf8') if not isinstance(name, bytes) else name
            output = dd_info()
            text = 64*b't'
            error   = ct.c_int32(0)
            _error  = ct.byref(error)
            _name   = ct.c_char_p(name)
            buffer  = (ct.c_int32*26)()
            _buffer = ct.byref(buffer)
            _text   = ct.c_char_p(text)
            lname = ct.c_uint64(len(name))
            ltext = ct.c_uint64(len(text))

            result = libddww.ddobjhdr_(_error, self._diaref, _name, _buffer, _text, lname, ltext)

            GetError(error)
            output.error = error.value
            if error.value == 0:
                output.buffer = buffer[0:26]
                output.text   = _text.value
            return output
        return None

    def GetObjectName(self, obj):
        if hasattr(self, '_diaref'):
            name = 8*b'1'
            error   = ct.c_int32(0)
            _error  = ct.byref(error)
            _name   = ct.c_char_p(name)
            obje    = ct.c_int32(obj)
            _obje   = ct.byref(obje)
            lname = ct.c_uint64(len(name))

            result = libddww.ddobjname_(_error, self._diaref, _obje, _name, lname)

            if error.value == 0:
                return _name.value.replace(b' ',b'')
        return -1

    def GetNames(self):
        if hasattr(self, '_diaref'):
            i = 1
            ok = True
            result = []
            while self.GetObjectName(i) != -1:
                result.append(self.GetObjectName(i))
                i += 1
            return result
        return None

    def GetDescr(self,name):
        if hasattr(self, '_diaref'):
            name = name.encode('utf8') if not isinstance(name, bytes) else name
            descr = b''
            for str in self.GetObjectValue(name,b'text'):
                descr += str
            return descr
        return None

    def GetRelations(self, name):

        if hasattr(self, '_diaref'):
            name = name.encode('utf8') if not isinstance(name, bytes) else name
            rel_out = dd_info()
            head = self.GetObjectHeader(name)
            rel_out.error = head.error
            if head.error == 0:
                ids = head.buffer[4:12]
                rel_out.id  = []
                rel_out.typ = []
                rel_out.txt = []
                for objid in ids:
                    if objid != 65535:
                        rel_out.id.append(objid)
                        tname = self.GetObjectName(objid)
                        rel_out.typ.append(self.GetObjectValue(tname, b'objtype'))
                        rel_out.txt.append(tname)
            return rel_out
        return None

    def GetObject(self, name, cal=False):

        name = name.encode('utf8') if not isinstance(name, bytes) else name
        info = self.GetInfo(name)

        output = info
        output.data = None
        output.area = None
        output.time = None

        if info.objtyp not in (6, 7, 8, 13):
            print('GetObject supports onle TB, AB, SIG or SGR')
            return None

        if info.tname != None:
            output.time  = self.GetTimebase(name)

        if info.objtyp == 8:
            output.data  = self.GetTimebase(name)

        if info.objtyp == 13:
            output.data  = self.GetAreabase(name)

        if info.objtyp == 7:
            output.data  = self.GetSignal(name, cal=cal)

        if info.objtyp == 6:
            output.data  = self.GetSignal(name, cal=cal)
            if info.aname != None:
                output.area  = self.GetAreabase(name)
                if info.index != None:
                    if info.index > 0:
                        output.data = output.data.T
            if output.area != None:
                arel = self.GetRelations(info.aname)
                if len(output.area.shape) == 1:
                    if 8 not in arel.typ:
                        output.area = np.tile(output.area, (len(output.time),1))
                    else:
                        output.area = np.reshape(output.area, newshape=(len(output.area),1))

        return output


def LastShotNr():
    """ nshot = dd.LastShotNr """
    error  = ct.c_int(0)
    _error = ct.byref(error)
    cshot  = ct.c_uint32(0)
    _cshot = ct.byref(cshot)

    result = libddww.ddlastshotnr_(_error, _cshot)

    return cshot.value

def PreviousShot(diagname, nshot, experiment='AUGD'):
    """ nshot = dd.PreviousShot(diagname, nshot) """
    diagname = diagname.encode('utf8') if not isinstance(diagname, bytes) else diagname
    experiment = experiment.encode('utf8') if not isinstance(experiment, bytes) else experiment
    exp    = ct.c_char_p(experiment)
    diag   = ct.c_char_p(diagname)
    cshot  = ct.c_uint32(0)
    _cshot = ct.byref(cshot)
    shot   = ct.c_uint32(nshot)
    _shot  = ct.byref(shot)
    lexp  = ct.c_uint64(len(experiment))
    ldiag = ct.c_uint64(len(diagname))

    result = libddww.ddcshotnr_(exp, diag, _shot, _cshot, lexp, ldiag)

    GetError(result)
    return cshot.value
