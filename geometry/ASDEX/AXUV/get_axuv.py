from collections import OrderedDict

import time 
import numpy as np
import aug_sfutils as sf


class loader_AXUV():
    """
    class for the loading of the AXUV data from AUG
    """

    n_smooth = 16
    
    def __init__(self, shot, geometry_path, experiment='AUGD',edition=0):
   
        """
        :var int shot:  Number of selected shot
        :var str geometry_path: Path to saved geometry, boundary or data
        :var object dd: loading library for AUG data
        :var str experiment: name of AUG  experiment
        :var int edition: edition of AUG  experiment
        """

        self.geometry_path = geometry_path

        self.shot = shot
        self.experiment = experiment
        self.ed = edition
        self.wrong_dets_damaged = []#BUG 
 
        calib_shot = sf.ddcshotnr('BLC', shot=self.shot)
        blc = sf.SFREAD('BLC', calib_shot)
        if not blc.status:
            #second try
            calib_shot = sf.ddcshotnr('BLC', shot=calib_shot-1)
            blc = sf.SFREAD('BLC', calib_shot)
            if not blc.status:
                raise Exception('No BLC shotfile!')

        self.all_names = [n for n in blc.sf.parsets if n[:2] == 'CS']

        self.calib    = {}
        self.offset   = {}
        self.channels = {}
        self.Cams     = {}
        self.active   = {}

        for name in self.all_names:
            ncalib = int(blc(name)['NCALSTEP'])
            multi = []
            offset = 0
            for i in range(ncalib):
                multi.append(blc(name)['MULTIA%.2d'%i])
                offset = offset*multi[-1] + blc(name)['SHIFTB%.2d'%i]
            self.calib[name[1:]]  = np.prod(multi)
            self.offset[name[1:]] = offset
            channel = blc(name)['Channel']
            camera  = blc(name)['Cam'].item()

            if channel != 0:
                if not camera in self.Cams:
                   self.Cams[camera] = []
                self.Cams[camera].append(name[1:])
                self.channels[name[1:]] = channel

        all_cams = np.unique(list(self.Cams.keys()))
        active_los = {s: np.bool_(blc(s)['active']) for s in all_cams}
        activ_cam = [c for c in all_cams if any(active_los[c]) and not c in ('DT1', 'DT3')]
        nlos =  [np.sum(active_los[c]) for c in activ_cam]
        ind = np.argsort(nlos)
        activ_cam = [activ_cam[i] for i in ind[::-1]]

        self.Phi     = {s: blc(s)['P_Blende'][active_los[s]] for s in activ_cam}
        self.R_start = {s: blc(s)['R_Blende'][active_los[s]] for s in activ_cam}
        self.z_start = {s: blc(s)['z_Blende'][active_los[s]] for s in activ_cam}
        self.R_end   = {s: blc(s)['R_end'   ][active_los[s]] for s in activ_cam}
        self.z_end   = {s: blc(s)['z_end'   ][active_los[s]] for s in activ_cam}
        self.theta   = {s: np.arctan2(self.z_end[s] - self.z_start[s], self.R_end[s] - self.R_start[s]) for s in activ_cam}
        self.delta   = {s: blc(s)['delta'][active_los[s]] for s in activ_cam}
        self.sig_dict= {s: [c for c in blc(s)['RAW']] for s in activ_cam}

        self.subcam_ind = {c:[self.delta[c] + self.R_start[c] == r for r in np.unique(self.delta[c]+self.R_start[c])] for c in activ_cam}
        self.DAS_limit = 2**14-1

        #geometric corrections  only estimated!!!
        n =  self.R_end['DHC']-self.R_start['DHC'], self.z_end['DHC']-self.z_start['DHC']
        corr = 0.04#rad
        self.R_end['DHC'][self.subcam_ind['DHC'][1]]+= -n[1][self.subcam_ind['DHC'][1]]*np.tan(corr)
        self.z_end['DHC'][self.subcam_ind['DHC'][1]]+= +n[0][self.subcam_ind['DHC'][1]]*np.tan(corr)
 
        n =  self.R_end['DVC']-self.R_start['DVC'], self.z_end['DVC']-self.z_start['DVC']
        corr = 0.02#rad
        self.R_end['DVC'][self.subcam_ind['DVC'][0]]+= -n[1][self.subcam_ind['DVC'][0]]*np.tan(corr)
        self.z_end['DVC'][self.subcam_ind['DVC'][0]]+= +n[0][self.subcam_ind['DVC'][0]]*np.tan(corr)
        
        self.activ_cam = activ_cam
   
        self.scam_ind       = OrderedDict()
        self.cam_ind        = OrderedDict()
        self.detectors_dict = OrderedDict()

        nlos = 0
        self.all_los = []
        for c in activ_cam:
            for isub,ind in enumerate(self.subcam_ind[c]):
                name = '' if len(self.subcam_ind[c])==1 else '_'+str(isub+1)
                self.scam_ind[c+name] = np.where(ind)[0]+nlos
            self.cam_ind[c] = nlos+np.arange(len(ind))
            nlos+= np.sum(active_los[c])
            self.detectors_dict[c] = [c+'_%d'%self.channels[n] for n in self.Cams[c]]
            self.all_los.extend(self.detectors_dict[c])

        self.nl = nlos
        self.activ_subcam = self.scam_ind.keys()

        self.calb_0 = np.ones(len(self.activ_subcam))
        self.dets = np.arange(self.nl)        
        self.dets_index = [v for k,v in self.cam_ind.items()]

        xvr = sf.SFREAD('XVR', self.shot)
        self.tvec = xvr('Dio-Time')
    
    def get_data(self,tmin=-np.infty,tmax=np.infty):
        """ Main diagnostics are diod Bolometers,
        """

        bolo_shotfiles = 'XVR', 'XVS'

        t_offset = -0.1
        nbeg,nend,noffset = self.tvec.searchsorted([tmin,tmax,t_offset])
        nend = min(nend+self.n_smooth//2, len(self.tvec)-1)
        nbeg = max(nbeg-self.n_smooth//2,0)
        #problems with numerical precision of the singles
        tvec = self.tvec[nbeg:nend]
        nt = nend-nbeg

        #load raw data
        data   = np.empty((     nt, self.nl), dtype=np.int16)
        offset = np.zeros((noffset, self.nl), dtype=np.int16)
        calib = np.zeros(self.nl)
        for bs in bolo_shotfiles:
            bol = sf.SFREAD(bs, self.shot)
            names = [n.strip() for n in bol.sf.objects if n[0] == 'S']
            for cam in self.activ_cam:
                ind = self.cam_ind[cam]
                signals = self.sig_dict[cam]
                for ilos,sig in zip(ind,signals):
                    sig = sig.strip()
                    if sig in names:
                        data  [:, ilos] = bol.getobject(sig, cal=False, nbeg=nbeg, nend=nend)
                        offset[:, ilos] = bol.getobject(sig, cal=False, nbeg=0, nend=noffset)
                        calib[ilos] = self.calib[sig]

        #averadge over "n_smooth" points 
        data = np.mean(data[:(nt//self.n_smooth)*self.n_smooth].reshape(nt//self.n_smooth, self.n_smooth,self.nl),1,dtype='float32')
        tvec = np.mean(tvec[:(nt//self.n_smooth)*self.n_smooth].reshape(nt//self.n_smooth, self.n_smooth),1)

        mad = lambda x: np.mean(abs(x-np.mean(x,0)),0)*1.4
        noise = mad(offset)
        offset = np.mean(offset,0)
        data -= offset
        data *= calib
        das_upper_limit = calib*(self.DAS_limit-offset)
        das_lower_limit = -calib*(offset)
        noise*= calib

        data_err = np.ones_like(data)
        data_err*= noise
        data_err[(data>das_upper_limit*0.99 )|(data<=das_lower_limit*0.99)] = np.infty  

        if self.shot > 31591:
            pass
        elif self.shot > 30135:
            pass
        elif self.shot > 28523:
            pass      
        elif self.shot > 27352:
    
            ind = self.scam_ind['DHC_2']
            data[:,ind]/= 1.05
            
            ind = self.scam_ind['DHC_3']
            data[:,ind]*= 1.3
            
            ind = self.scam_ind['DHC_1']
            data[:,ind]*= 1.1

        return tvec, data, data_err

 
    def load_geom(self, path):

        for i,det in enumerate(self.activ_cam):
            R1 = self.R_start[det]
            Z1 = self.z_start[det]
            R2 = self.R_end[det]  
            Z2 = self.z_end[det]  
            savetxt(self.geometry_path+'/detector_%s_x.txt'%det,c_[R2,R1],fmt='%.4f')
            savetxt(self.geometry_path+'/detector_%s_y.txt'%det,c_[Z2,Z1],fmt='%.4f')
