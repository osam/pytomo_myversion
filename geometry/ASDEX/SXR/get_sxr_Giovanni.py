from scipy.interpolate import interp1d
import os, sys, logging
from collections import OrderedDict 
import numpy as np
from shared_modules import fast_svd
import aug_sfutils as sf

fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')
hnd = logging.StreamHandler()
hnd.setFormatter(fmt)
logger = logging.getLogger('get_sxr')
logger.addHandler(hnd)
logger.setLevel(logging.DEBUG)
#logger.setLevel(logging.INFO)


class loader_SXR():
    """
    class for the loading of the SXR  data from AUG
    """

    sigma = 0.02
    def __init__(self, shot, geometry_path, fast_data=False, experiment='AUGD', edition=0):

        """
        :var int shot:  Number of selected shot
        :var str geometry_path: Path to saved geometry, boundary or data
        :var str experiment: name of AUG  experiment
        :var int edition: edition of AUG  experiment
        """

        self.shot = shot
        self.experiment = experiment
        self.fast_data = fast_data
        self.ed = edition
        self.geometry_path = geometry_path

        self.calib_shot = sf.previousshot('CSX', shot=self.shot)
        logger.debug('Calib shot: %d %d', self.shot, self.calib_shot)
        csx = sf.SFREAD( 'CSX', self.calib_shot)
        names = csx.sf.objects + csx.sf.parsets

        signals = [b[1:] for b in names if b[0] == 'C']
        shotfiles = []
        self.geometry = {}
        self.status   = {}
        self.SampFreq = {}
        self.ADCrange = {}
        self.different_det = {}

        geometry_names = ('Tor_Pos', 'RPINHOLE', 'ZPINHOLE', 'REND', 'ZEND', 'D_Width',
                    'D_Length', 'THETA', 'CAMANGLE', 'D_Gap', 'P_Width', 'P_Length', 'Foc_Len')
        for name in geometry_names:
            self.geometry[name] = {}
                    
        for s in signals:
            parset = csx('C' + s)
            shotfiles.append(sf.str_byt.to_str(  b''.join(parset['SX_DIAG']) ) )
            for name in geometry_names:
                self.geometry[name][s] = parset[name]
            self.status[s]   = parset['ADDRESS'] != 256
            self.SampFreq[s] = parset['SampFreq']
            filt_mat         = sf.str_byt.to_str(parset['FILT-MAT'].item())
            thickness        = parset['THICKNES']
            self.ADCrange[s] = parset['ADCrange']
            self.different_det[s] = (abs(thickness - 75e-6) > 1e-5) | (filt_mat != 'Be')

        self.ADCmin = 0

        self.MULTIA = {}
        self.SHIFTB = {}

        for k, s in enumerate(signals):
            parset = csx('C' + s)
            n = int(parset['NCALSTEP'])
            self.MULTIA[s] = [parset['MULTIA%.2d' %i] for i in range(n)]
            self.SHIFTB[s] = [parset['SHIFTB%.2d' %i] for i in range(n)]

        self.SXR_diods = OrderedDict()
        for sfile in np.unique(shotfiles):
            self.SXR_diods[sfile] = []

        for sfile,  sig in zip(shotfiles, signals):
            if sig[0] != 'T': #remove T camera
                self.SXR_diods[sfile].append(sig)
            
        self.SXR_diods.pop('OOO')  #null detector

        #identify a single subcameras of the SXR system
        self.all_los = np.hstack(list(self.SXR_diods.values()))
        self.all_los.sort()

        cams, index = np.unique([l[0] for l in self.all_los], return_inverse=True)
        CANGLE = np.array([self.geometry['CAMANGLE'][l] for l in self.all_los])
        self.detectors_dict=OrderedDict()
        for i, c in enumerate(cams):
            uangle, sub_ind, sub_index = np.unique(CANGLE[index == i], return_inverse=True, return_index=True)
            n_cams = len(uangle)
            if n_cams == 1:
                self.detectors_dict[c] = self.all_los[index == i]
            else:
                sort_ind = np.argsort(sub_ind)
                for j in range(n_cams):
                    self.detectors_dict[c+str(j+1)] = self.all_los[index == i][sub_index == sort_ind[j]]
        
        #find channels corresponding to the each subcamera
        self.cam_ind = OrderedDict()
        for k, item in self.detectors_dict.items():
            self.cam_ind[k] = np.zeros(len(item), dtype=int)
            for i, los in enumerate(item):
                self.cam_ind[k][i] = np.where(self.all_los == los)[0][0]

        self.nl = len(self.all_los)
        self.calb_0 = np.ones(len(list(self.detectors_dict.keys())))
        self.Ndets = len(list(self.detectors_dict.keys()))
        self.dets_index = [v for k, v in self.cam_ind.items()]
        self.dets = np.arange(self.nl)

        try:
            SXerrors = np.genfromtxt('/shares/departments/AUG/users/sxry/SXR/errdata/SXerrors/SX_err_%d.dat'%self.shot,
                        dtype={'names': ('channel', 'Diag', 'signal', 'error', '13or14', '15or16'),
                                'formats': ('U5', 'U3', 'i4', 'i4', 'i4', 'i4')}, usecols=(0, 1, 2, 3, 4, 5))
            print('\n\nSuspicious channels from /home/s/sxry/SXR/errdata/SXerrors/SX_err_%d.dat'%self.shot)
            print('\t'.join(['channel', 'name', 'errors', '13or14', '15or16']))
            
            CSI="\x1B["
            reset=CSI+"m"
            red_start = CSI+"31;40m"
            red_end = CSI + "0m" 
            wrong_ch = []
            for i, los in enumerate(self.all_los):
                for err in SXerrors:
                    if los == err[0]: 
                        if err[3] < 0 or err[3] > 10000:
                            print(red_start, end=' ')
                            wrong_ch.append(i+1)
                        print(i+1, '\t', err[0], '\t', err[3], '\t', err[4], '\t', err[5], end=' ')
                        if err[3] < 0 or err[3] > 10000: print(red_end)
                        else: print() 
                        
            print('Possibly damaged channels')
            print(wrong_ch)
                        
        except Exception as e:
            print(e)
            pass
     
        self.Phi = [np.deg2rad( self.geometry['Tor_Pos'][s]+45) for s in self.all_los]#add 45deg to make tor postion consistent with diaggeom
     
        if fast_data:
            sxa = sf.SFREAD('SXA', self.shot)
            if not sxa.status:
                raise Exception('SXA shotfile is not avalible')
            self.tvec = sxa('Time')
        else:
            ssx = sf.SFREAD('SSX', self.shot, exp=self.experiment, ed=self.ed)
            if not ssx.status:
                raise Exception('SSX shotfile is not avalible')
            self.tvec = ssx('time')


    def get_data(self, tmin=-np.infty, tmax=np.infty):

        if self.fast_data:
            return self.get_data_fast(tmin, tmax)

        ssx = sf.SFREAD('SSX', self.shot, exp=self.experiment, ed=self.ed)
        tvec  = self.tvec
        tvec2 = ssx('time2')

        imin, imax = tvec.searchsorted([tmin, tmax])
        if imax != len(tvec): imax+= 1
        
        imin2, imax2 = tvec2.searchsorted([tmin, tmax])
        if imin2 == imax2: imax2 += 2
        if imax2 != len(tvec2): imax2 += 1

        tvec  = tvec[ imin:imax]
        tvec2 = tvec2[imin2:imax2]

        signals = ssx.sf.objects + ssx.sf.parsets

        #load uncalibrated data - it is  faster!!
        data = np.empty((imax-imin, self.nl), dtype=np.int16)
        for ilos, los in enumerate(self.all_los):
            tmp = ssx.getobject(los, cal=False, nbeg=imin, nend=imax)
            if tmp is not None:
                data[:, ilos] = tmp

        #identify wrong time points 
        ADCrange = np.array([self.ADCrange[s] for s in self.all_los])
        wrong_poinstDAS = data > ADCrange #DAS failures 
        wrong_poinst = (data == self.ADCmin) | (data >= ADCrange)  #points out of DAS range

        offset = np.zeros(self.nl, dtype=np.single)
        calib  = np.ones( self.nl, dtype=np.single)

        for j, name in enumerate(self.all_los):
            M = self.MULTIA[name]
            S = self.SHIFTB[name]
            offset[j] = ((S[0]*M[1]+S[1])*M[2]+S[2])*M[3]+S[3]
            calib[j] = np.prod(M)
        
        #calibrate signals
        data = np.single(data)
        data *= calib
        data += offset

        data_min, data_max = None, None
        if self.all_los[0] + '_MN' in signals:
            data_min = np.empty((imax2-imin2, self.nl), dtype=np.int16)
            data_max = np.empty((imax2-imin2, self.nl), dtype=np.int16)
            for ilos, los in enumerate(self.all_los):
                tmp = ssx.getobject(los+'_MN', cal=False, nbeg=imin2, nend=imax2)
                if tmp is not None:
                    data_min[:, ilos] = tmp
                tmp = ssx.getobject(los+'_MX', cal=False, nbeg=imin2, nend=imax2)
                if tmp is not None:
                    data_max[:, ilos] = tmp

        sigma_name = 'Sm' if not self.experiment in ['SXRY', 'TODSTRCI'] and self.shot < 30619  else 'Sn'
        data_err = None
        if self.all_los[0]+'_'+sigma_name in signals:  
            data_err = np.empty((imax2-imin2, self.nl), dtype=np.int16)
            for ilos, los in enumerate(self.all_los):
                tmp = ssx.getobject(los+'_'+sigma_name, cal=False, nbeg=imin2, nend=imax2)
                if tmp is not None:
                    data_err[:, ilos] = tmp

        low_data = data
                
        if len(tvec) != len(tvec2):
            trimmed_tvec = maximum(minimum(tvec[-1], tvec2), tvec[0])
            low_data = interp1d(tvec, data, axis=0,
                            kind='nearest', assume_sorted=True)(trimmed_tvec)  

        #estimate the errorbars
        if data_err is None and not data_max is None:
            #very old shotfile - naive guess of the errobars 
            data_err = abs(np.single(data_max-data_min))
            data_err[data_max >= ADCrange] = np.infty
        elif np.all(data_err == 0):
            #corrupted shotfile 
            data_err = data*np.single(self.sigma)

        elif not data_err is None and np.size(data_err) == self.nl*len(tvec2):

            #standard shotfile
            data_err  = np.single(data_err)
            data_err *= calib
            
            #compansate overetimated errobars for slow DAS
            SampFreq = np.array([self.SampFreq[s] for s in self.all_los])
            data_err*= SampFreq/2.e6

            if not self.experiment in ['SXRY', 'MARKUSW', 'TODSTRCI'] and self.shot < 30619:
                #old shotfile, a different algorithm to calculate data_err was used
                data_err = np.minimum(data_err, (data_max-data_min)*np.single(calib)/2)#better error estimate in the presence of the MHD mode
                #values too close to minimum or maximum are wrong  in old shotfiles due to limited ADC range 
                data_err[((data_min == self.ADCmin)&((low_data-offset) < 2*data_err))|(data_max >= ADCrange)] = np.infty

        else:
            #when the STD is not avalible - use a guess from min and max
            data_err = np.single(np.minimum(low_data-data_min*calib-offset, data_max*calib+offset-low_data)/3)
        
        if len(tvec) != len(tvec2):
            trimmed_tvec = np.maximum(np.minimum(tvec2[-1], tvec), tvec2[0])
            data_err = interp1d(tvec2, 1./data_err/3, axis=0, bounds_error=False,
                            fill_value=np.infty, kind='nearest', assume_sorted=True)(trimmed_tvec)

            data_err[data_err != 0] = 1/data_err[data_err != 0] 
            data_err = np.single(data_err)
 
        data_err[wrong_poinst|np.isnan(data_err)|(data_err <= 0)] = np.infty
        data[wrong_poinstDAS] = 0

        useless_det = np.where([~self.status[s]|self.different_det[s] for s in self.all_los])[0]

        self.wrong_dets_damaged = self.all_los[useless_det]    
        self.hardcoded_corrections(tvec,  data, data_err)
        
        return tvec, data, data_err


    def hardcoded_corrections(self, tvec,  data, data_err):
        
        if self.shot >= 30420 and self.shot <= 31900: 
            self.wrong_dets_damaged = np.r_[self.wrong_dets_damaged, ['H_%.3d'%i for i in range(46, 81)]]

        if self.shot >= 30420 and self.shot <= 30447: 
            self.wrong_dets_damaged = np.r_[self.wrong_dets_damaged, ['H_%.3d'%i for i in range(0, 100)]]

        if self.shot >= 25995 and self.shot <= 27392:  #wrong BE filter
            self.wrong_dets_damaged = np.r_[self.wrong_dets_damaged, ['H_%.3d'%i for i in range(17, 26)]]
        elif self.shot <= 29500:
            pass

        elif self.shot < 31776:
            self.wrong_dets_damaged = np.r_[['I_061', ], self.wrong_dets_damaged]
            #geometry correction, 
            L_calib = np.r_[1.446, 1.426, 1.248, 1.183, 1.093]
            data[:, self.cam_ind['L'][:len(L_calib)]] *= L_calib

        elif self.shot < 33724:
            #etendue correction, 
            L_calib = np.r_[1.446, 1.426, 1.248, 1.183, 1.093]
            M_calib = np.r_[1.900, 2.18, 2.28, 2.67, 3.95]
            if 'L' in self.cam_ind: data[:, self.cam_ind['L'][:len(L_calib)]] *= L_calib
            if 'M' in self.cam_ind: data[:, self.cam_ind['M'][-len(M_calib):]] *= M_calib
            self.wrong_dets_damaged = np.r_[self.wrong_dets_damaged, ['K_021', ]]

        elif self.shot < 34000: #I don;t know when they have repaired it 
            self.wrong_dets_damaged = np.r_[self.wrong_dets_damaged,
                                         ['H_048', 'H_050', 'H_052', 'H_054',
                                          'K_020', 'K_014', 'K_015', 'K_016', 
                                          'K_017', 'K_019','M_014', 'H_058','H_022']]
        else:
            #pass
            L_calib = np.r_[1.1, 1.2, 1.4 ]
            #M_calib = np.r_[1.900, 2.18, 2.28, 2.67, 3.95]
            if 'L' in self.cam_ind: data[:, self.cam_ind['L'][-len(L_calib):]] /= L_calib
            #if 'M' in self.cam_ind: data[:, self.cam_ind['M'][-len(M_calib):]] *= M_calib
  
        if self.shot < 27000  and self.shot > 22000 :
            #correct the wrongly estimated offset
            if np.any(tvec < 0.01):
                ind = np.any(np.isfinite(data_err[tvec < 0.01]), axis=0)
                offset = np.nanmedian(data[:tvec.searchsorted(.01), ind], axis=0)
            else:
                _, data_0, data_err_0 = self.get_data(tmin=-np.infty, tmax=0.01)

                ind = np.any(np.isfinite(data_err_0), axis=0)
                offset = np.nanmedian(data_0[:, ind], axis=0)

            data[:, ind] -= offset


    def get_data_fast(self, tmin=-np.infty, tmax=np.infty):

        import aug_sfutils as sf

        wrong_det = []

        if tmax < 0: raise
        imin_, imax_ = self.tvec.searchsorted((tmin, tmax))
        tvec = self.tvec[imin_:imax_+1]
            
        ndets = 0
        isig = 0
        all_data = np.zeros((len(tvec), self.nl), dtype=np.single)
        
        detector_lim   = np.zeros(self.nl)
        detector_stat  = np.zeros(self.nl, dtype=bool)
        discr_err      = np.zeros(self.nl)  #discretisation error
        detector_sampl = np.zeros(self.nl)

        for DAS, signals in self.SXR_diods.items():

            if len(signals) == 0:
                continue

            logger.info("\rloading %2.0f%%  diag:%s  ", isig*100./self.nl, DAS)

            isig += len(signals)
            ndets += 1

            sfo = sf.SFREAD(DAS, self.shot)
            if not sfo.status:
                continue

            los_num = self.all_los.searchsorted(signals)
            ADCrange = self.ADCrange[signals[0]]
            SampFreq = self.SampFreq[signals[0]]

            offset = np.zeros(len(signals))
            calib  = np.ones(len(signals))
            for j, name in enumerate(signals):
                M = self.MULTIA[name]
                S = self.SHIFTB[name]
                offset[j] = ((S[0]*M[1] + S[1])*M[2] + S[2])*M[3] + S[3]
                calib[j] = np.prod(M)

            tb = sfo('Time')
            tbeg = tb[0]
            tend = tb[-1]
            tlen = len(tb)

            imin, imax = (np.r_[tmin,  tmax] - tbeg)/(tend - tbeg)*(tlen - 1)
            imin, imax = int(np.ceil(imin)), int(np.ceil(min(imax, tlen)))
            
            data_tvec = tb[imin: imax+1]
            nt = len(data_tvec)

            #load data
            data = np.ma.zeros((nt, len(signals)), dtype=np.int16)
            for i, sig in enumerate(signals):
                data[:,  i] = sfo.getobject(sig, cal=False, nbeg=imin, nend=imax+1)

            ##wrong poinst, DAS failure - usually single time points 
            wrong_ind = np.where(data > ADCrange)
            for i, j in zip(*wrong_ind): 
                data[i, j] = 0 if i == 0 else data[i-1, j]
            
            dt = 1./SampFreq
            Nf = int(1e-3*SampFreq)

            data.mask = data <= 0  #get rid of wrong points and cut of points

            data_ = data[:(nt//Nf)*Nf].reshape(Nf, -1, np.size(data, 1))
            data_.mask = data.mask[:(nt//Nf)*Nf].reshape(Nf, -1, np.size(data, 1))

            #compensate the bottom cutted signal by cutting of the tops - averadge will be correct
            for i in range(np.size(data_, 1)):
                for j in range(np.size(data_, 2)):
                    d = data_[:, i, j]
                    if not np.any(d.mask) or np.all(d.mask):   continue
                    q = np.sum(d.mask)/float(np.size(data_, 0))
                    d.mask[d >= np.percentile(d.data[~d.mask], (1-q)*100)] = True
        
            #overburned poinst
            data.mask[data.data == ADCrange] = False
            #negative points are corrupted measurements!
            data.mask[data.data == 0]        = False

            #apply the calibration and shift of the detectors
            data = np.single(data)
            data *= calib
            data += offset
            
            #interpolate on the full time resolution and fill corrupted points 
            for i, sig in enumerate(signals):
                valid = ~data.mask[:, i]
                if abs(len(data_tvec)- len(tvec)) < 0.01*len(tvec) and abs(len(data_tvec)- len(tvec)) != 0:

                    raise Exception('Wrong length of tvec!! %d  %d'%(len(data_tvec), len(tvec)))

                if (len(tvec) != len(data_tvec) or not np.all(valid)) and np.any(valid): 
                    all_data[:, los_num[i]] = np.interp(tvec, data_tvec[valid], data[:, i].data[valid])
                elif np.any(valid):
                    all_data[:, los_num[i]] = data[:, i].data
                    
                detector_stat[los_num[i]]  = self.status[sig]&~self.different_det[sig]

            detector_lim[los_num]   = ADCrange*calib+offset
            discr_err[los_num]      = calib
            detector_sampl[los_num] = SampFreq

        self.dets_index = [np.array(v) for k, v in self.cam_ind.items()]
        calib = np.ones(len(self.cam_ind))

        wrong_det = self.all_los[~detector_stat]
        self.wrong_dets_damaged = wrong_det
        print('wrong_dets_damaged:', wrong_det)

        #estimate errorbars!!
        nt = len(tvec)
        ind =  ~np.in1d(self.all_los, self.wrong_dets_damaged)
        wrong_data_ind = np.isnan(all_data)
        all_data[wrong_data_ind] = 0
  
        ind_t = slice(0, nt)
        if nt > 2000:  ind_t = np.r_[0, np.unique(np.random.randint(nt, size=1000)), nt-1]
        
        U, S, V = fast_svd(all_data[ind_t][:, ind], min(30, nt//3))
        
        #assume that the differnce between data and SVD retrofit is only noise 
        svd_err = np.ones(len(self.dets))*np.infty
        svd_err[ind] = np.std(all_data[ind_t][:, ind]-np.dot(U, V*S[:, None]), 0)

        wrong_data_ind |= np.all(all_data == np.mean(all_data, 1)[:, None], 1)[:, None]
        wrong_data_ind |= all_data > detector_lim-1 #overburned

        #constant noise for all timepoints!
        all_data_err = np.zeros_like(all_data)
        all_data_err[:] = 2*svd_err+discr_err

        all_data_err[wrong_data_ind] = np.infty

        #mostly overburned values and missing detectors - just for plotting!
        for i in range(self.nl):  
            if np.any(np.isnan(all_data[:, i])):
                all_data[:, i][np.isnan(all_data[:, i])] = detector_lim[i]
   
        #apply special corrections
        self.hardcoded_corrections(tvec, all_data, all_data_err)

        return tvec, all_data, all_data_err


    def load_geom(self, path):
        
        #separate equlibrium for each campaign 
        ##load corrections in degrees of the camera position

        self.geometry_version = 0
        corrections = 'det_pos_corr_null'

        if self.shot > 34200:#just guess
            self.geometry_version = 10
            #corrections =  'det_pos_corr_2017_3'
            corrections =  'det_pos_corr_2019_1'

        elif self.shot > 34007:
            self.geometry_version = 9
            corrections =  'det_pos_corr_2017_2'

        elif self.shot > 33800:
            self.geometry_version = 8
            corrections =  'det_pos_corr_2017'

        elif self.shot > 31776:
            self.geometry_version = 7
            corrections =  'det_pos_corr_new'

        elif self.shot > 27439:
            self.geometry_version = 6
            corrections =  'det_pos_corr'

        elif self.shot > 25600:
            self.geometry_version = 5
            corrections =  'det_pos_corr_old'

        elif self.shot > 24916:
            self.geometry_version = 4
            corrections =  'det_pos_corr_old3'

        elif self.shot >18000:
            self.geometry_version = 3
            
        elif self.shot > 13000:
            self.geometry_version = 2
            
        elif self.shot < 1000:  #artificial discharges!!
            self.geometry_version = 1
            corrections = 'det_pos_corr_null'

        if not hasattr(self, 'geometry'):
            raise Exception('No geometry data')

        pos_corr = loadtxt(path+'/'+corrections+'.txt',
                           dtype={'names': ('det', 'angle'), 'formats': ('U2', 'f4')})
        pos_corr =  {k:item for k, item in pos_corr}

        Phi = []
        coord_dict = OrderedDict()

        #prepare files with cordinates
        for icam, (det, signals) in enumerate(self.detectors_dict.items()):
            xfile = open(self.geometry_path+'/detector_%s_x.txt'%det, 'w')
            yfile = open(self.geometry_path+'/detector_%s_y.txt'%det, 'w')
            dist_file = open(self.geometry_path+'/dist_%s.txt'%det, 'w')

            coord_dict[det] = []
            verts = []

            color = ('r', 'b', 'g', 'k', 'm', 'y', 'k', 'gray', 'r', 'm', 'k', 'b', 'g', 'y', 'k')
         
            m_alpha = []
            for sig in signals:
                r1 = self.geometry['RPINHOLE'][sig]
                z1 = self.geometry['ZPINHOLE'][sig]
                r2 = self.geometry['REND'][sig]
                z2 = self.geometry['ZEND'][sig]
                m_alpha.append(abs(abs(np.arctan2(z2-z1, r2-r1))-np.pi/2))
                Phi.append( self.geometry['Tor_Pos'][sig]+45)#add 45deg to make tor postion consistent with diaggeom

            m_alpha = np.mean(m_alpha)

            for sig in signals:
                r1 = self.geometry['RPINHOLE'][sig]
                z1 = self.geometry['ZPINHOLE'][sig]
                r2 = self.geometry['REND'][sig]
                z2 = self.geometry['ZEND'][sig]
                coord_dict[det].append([[r1, r2, z1, z2]])
         
                THETA    = self.geometry['THETA'][sig]
                CAMANGLE = self.geometry['CAMANGLE'][sig]
                
                dAngle = np.deg2rad(THETA-CAMANGLE)
                if dAngle > np.pi:
                    dAngle-= 2*np.pi

                alpha = np.arctan2(z2-z1, r2-r1)
            
                if self.shot > 1000 and self.shot > 20000 and det  in pos_corr:
                    #print 'poss corr', pos_corr
                    alpha+= np.deg2rad(pos_corr[det])

                L = np.hypot(r2-r1, z2-z1)
                if L == 0:
                    L = 1
                    alpha = 1

                #camera geometry 
                Dx    = self.geometry['D_Width' ][sig] # 0.96 mm
                Delta = self.geometry['Foc_Len' ][sig] # 14.#mm
                Px    = self.geometry['P_Width' ][sig] # 0.3#mm
                Dy    = self.geometry['D_Length'][sig] # 4.6#mm
                Py    = self.geometry['P_Length'][sig] # 5.0#mm
                Delta1 = Px/Dx*Delta/(1+Px/Dx)
                theta = np.arctan(Px/2/Delta1)

                #angle between axis of the camera and LOS
                theta*= np.cos(dAngle)
                delta = np.tan(Dy/2./Delta)
                delta*= 0.5
                dist = r1*np.sin(np.arctan(delta/np.cos(np.arctan((z2-z1+1e-6)/(r2-r1+1e-6)))))
                dist_file.write('%5.4f\n'%abs(dist))

                if m_alpha < np.pi/4:       
                    Lr = L*abs(np.sin(alpha)/np.sin(np.pi-theta-alpha))
                    Ll = L*abs(np.sin(np.pi-alpha)/np.sin(alpha-theta))

                    xfile.write('%5.4f %5.4f %5.4f\n'%(r1+Ll*np.cos(alpha-theta), r1+Lr*np.cos(alpha+theta), r1))
                    yfile.write('%5.4f %5.4f\n'%(z1+Lr*np.sin(alpha+theta), z1))
                    
                    verts.append([[r1, z1], [r1+Ll*np.cos(alpha-theta), z1+Lr*np.sin(alpha+theta) ]
                                    , [r1+Lr*np.cos(alpha+theta), z1+Lr*np.sin(alpha+theta)]])

                else:
                    if np.tan(np.pi-abs(alpha)-theta)*np.tan(np.pi-abs(alpha)+theta) < 0 and np.sin(np.pi/2+alpha) > 0:
                        #solve special case for almost exactly vertical LOS
                        z21 = z2  
                        z22 = z2+1e-2
                    else:
                        z21 = z1-(r2-r1)*np.tan(np.pi-abs(alpha)-theta)*np.sign(alpha)
                        z22 = z1-(r2-r1)*np.tan(np.pi-abs(alpha)+theta)*np.sign(alpha)
                     
                    yfile.write('%5.4f %5.4f %5.4f\n'%(z21, z22, z1))
                    xfile.write('%5.4f %5.4f\n'%(r2, r1))
                    verts.append([[r1, z1], [r2, z21], [r2, z22]])

            xfile.close()
            yfile.close()
