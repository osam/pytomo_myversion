#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys
from collections import OrderedDict
import numpy as np
from annulus import get_bd_mat
import aug_sfutils as sf


def ReadTomo(path, diag, shot, time,geometry_path):

    f = open(path+'resultef2d.%s.%d_%.6f'%(diag, shot, time))
    diaf, shot_, time_ = f.readline().split()
    ed = f.readline()
    nx,ny = f.readline().split()
    nx,ny = int(nx)+1, int(ny)+1
    R = np.array([float(f.readline().replace('D','E')) for i in range(nx)])
    z = np.array([float(f.readline().replace('D','E')) for i in range(ny)])
    E = [float(f.readline()) for i in range(ny*nx)]
    dR = np.mean(np.diff(R))
    dz = np.mean(np.diff(z))

    E = np.reshape(E, (nx, ny)).T
    ndets  = int(f.readline())
    data,retro,err,fact = np.array([np.float_(f.readline().split()) for i in range(ndets)]).T
    err[err<0] = np.infty
    err*= data
    r1, r2 = f.readline().split()
    chi2 = float(f.readline())

    f = open(path+'paramsef2d.BLB.%d_%.6f'%(shot, time))

    nx,ny,ver = f.readline().split()
    nx,ny = int(nx)+1, int(ny)+1

    R_ = [float(f.readline()) for i in range(nx)]
    z_ = [float(f.readline()) for i in range(ny)]

    psi, par, per = np.array([np.float_(f.readline().split()) for i in range(ny*nx)]).T
    psi = np.reshape(psi, (nx, ny))
    par = np.reshape(par, (nx, ny))
    per = np.reshape(per, (nx, ny))

    ed2 = f.readline()
    ndets = int(f.readline())

    r1,z1,psi,d,theta,f,_,_,_,_,data_,gain,used,fact_ = np.array([np.float_(f.readline().split()) for i in range(ndets)]).T

    border = np.loadtxt(geometry_path+'/border.txt')
    from scipy.ndimage.interpolation import zoom

    #zoom to reduce discretisation error when the radiation outside of the borders is set to zero
    n = 10
    E = zoom(E, n,order=0)
    R = zoom(R, n,order=0)
    z = zoom(z, n,order=0)
    dR = np.mean(np.diff(R))
    dz = np.mean(np.diff(z))

    bbox = R[0]-dR/2, R[-1]+dR/2, z[0]-dz/2, z[-1]+dz/2
    BdMat = get_bd_mat(tokamak=None, nx=nx*n, ny=ny*n, boundary=border, bbox=bbox)
    
    E[BdMat.reshape(ny*n, nx*n,order='F')] = 0

    power = 2*np.pi*sum(E*R[None])*dR*dz

    return R, z, E, data_*fact,retro,err, power, bbox, border,fact

    
class loader_BOLO():

    
    """
    class for the loading of the foil bolometer data from AUG

    """
    
    
    dt = 0.001
    sigma = 0.00
    min_error = 0.02
    
    def __init__(self, shot, geometry_path, experiment='AUGD',edition=0):
        
        """

        :var int shot:  Number of selected shot
        :var str geometry_path: Path to saved geometry, boundary or data
        :var object dd: loading library for AUG data
        :var str experiment: name of AUG  experiment
        :var int edition: edition of AUG  experiment

        """
        self.geometry_path = geometry_path
        self.shot = shot
        self.experiment = experiment
        self.ed = edition
        self.wrong_dets_damaged = []#BUG

        calib_shot = sf.ddcshotnr('BLC', shot=self.shot)
        print('DEBUG cal blc', calib_shot)
        
        blc = sf.SFREAD('BLC', calib_shot)

        if not blc.status:
            #second try
            calib_shot = sf.ddcshotnr('BLC', shot=calib_shot-1)
            blc = sf.SFREAD('BLC', calib_shot)
            if not blc.status:
                raise Exception('No BLC shotfile!')

        cams = [b for b in blc.sf.parsets if b[0] == 'F' and len(b) == 3]
   
        active_los = {s: np.bool_(blc(s)['active']) for s in cams}
        activ_cam = [c for c in cams if any(active_los[c])]
        try:
            activ_cam.remove('FEC')
        except:
            pass
        # [osam]: remove None type data 
        try:
            blb = sf.SFREAD('BLB', self.shot)
            if not blb.status:
                raise Exception('no bolometric data')
            for i, det in enumerate(activ_cam):
                if blb.gettimebase('tim'+det) is None:
                    activ_cam.remove(det)
                    print(f"camera={det} is removed (None type data)")
        except:
            print("Failure to remove None types of detectors")

        self.Phi     = {s: blc(s)['P_Blende'][active_los[s]] for s in activ_cam}
        self.R_start = {s: blc(s)['R_Blende'][active_los[s]] for s in activ_cam}
        self.z_start = {s: blc(s)['z_Blende'][active_los[s]] for s in activ_cam}
        self.R_end   = {s: blc(s)['R_end'   ][active_los[s]] for s in activ_cam}
        self.z_end   = {s: blc(s)['z_end'   ][active_los[s]] for s in activ_cam}
        self.delta   = {s: blc(s)['delta'   ][active_los[s]] for s in activ_cam}
        self.nchan   = {s: blc(s)['N_chan'] for s in activ_cam}
        self.raw_name= {s: blc(s)['RAW'   ] for s in activ_cam}

        self.theta = {s:np.arctan2(self.z_end[s]-self.z_start[s],self.R_end[s]-self.R_start[s]) for s in activ_cam}
        self.subcam_ind = {c:[self.R_start[c] == r for r in np.unique(self.R_start[c])] for c in activ_cam}
        self.channels = {s:np.arange(len(a))[a]  for s,a in  active_los.items()}

        self.activ_cam  = activ_cam
        self.active_los = active_los
   
        self.detectors_dict = OrderedDict()
        
        self.cam_ind = OrderedDict()
        nlos = 0
        self.all_los = []
        for c in activ_cam:
            self.cam_ind[c] = nlos+np.arange(sum(active_los[c]))
            nlos+= sum(active_los[c])
            self.detectors_dict[c] = [c+'_%d'%ch for ch in self.channels[c]]
            self.all_los.extend(self.detectors_dict[c])
        self.nl = nlos

        self.calb_0 = np.ones(len(activ_cam))
        self.dets = np.arange(self.nl)
        self.dets_index = [v for k,v in self.cam_ind.items()]

        blb = sf.SFREAD('BLB', self.shot, experiment=self.experiment, edition=self.ed)
        if not blb.status:
            raise Exception('Bolometric data do not exists!!!')
        tvec = blb('timFVC')
        self.tvec = np.arange(tvec[0],tvec[-1], self.dt )  #timevector for dowsampled data
        
        if self.shot > 33800 :
            self.wrong_dets_damaged += [  'FDC_4',]


    def get_total_rad(self):
        

        def reduce(x,y,n=1000):
            r = len(x)//n
            if r > 1:
                nt = len(x)
                x = np.mean(x[:(nt//r)*r].reshape(-1, r),1)
                y = np.mean(y[:(nt//r)*r].reshape(-1, r),1)
            return x,y

        out = []

        tot = sf.SFREAD('TOT', self.shot)
        if tot.status:
            P_TOT = tot('P_TOT')
            P_TOT_tvec = tot.gettimebase('P_TOT')
            P_TOT_tvec, P_TOT = reduce(P_TOT_tvec, P_TOT)
            out.append(['TOT:P_TOT', P_TOT_tvec, P_TOT])

        bpd = sf.SFREAD('BPD', self.shot)
        if bpd.status:
            for sig in ('Pradtot', 'Prad'):
                if sig in bpd.sf.objects:
                    Prad = bpd.getobject(sig, cal=False)
                    Prad_tvec = bpd.gettimebase(sig)
                    out.append(['BPD:'+sig, Prad_tvec, Prad])

        from os import listdir
        path = '/shares/departments/AUG/users/mbern/entfaltungen/data/'

        folders = listdir(path)
        folders.sort()
        # shot_folders = {}
        ed  = 0
        times= []
        diag = None
        for folder in folders:
            try:
                diag_, shot_ = folder.split('.')
                shot_, ed_ = shot_.split('_')
                shot_, ed_ = int(shot_),int(ed_)
            except:
                continue

            if self.shot != shot_ :continue

            files =  listdir(path+'/'+folder)
            times_ = []
            for file in files:
                if file.startswith('resultef2d'):
                    times_.append(float(file.split('_')[1]))
            
            if len(times_) > len(times) or ((len(times_) == len(times)) and (ed_>ed)):
                times = times_
                ed = ed_
                diag = diag_
        
        if not diag is None:
            total_power = []
            for time in times:
                folder = path+diag+'.%d_%d/'%(self.shot,ed)
                R, z, E, data,retro,err, power,bbox, border,fact = ReadTomo(folder, diag, self.shot, time, self.geometry_path)
                total_power.append(power)
            print( "loaded M. Bernert's tomography ed:%d  times:"%ed, times)
            out.append(['BLB:mbern', times,np.array(total_power)])

        return out

      
    def get_data(self,tmin=-np.infty,tmax=np.infty):

        blb = sf.SFREAD('BLB', self.shot, experiment=self.experiment, edition=self.ed)
        if not blb.status:
            raise Exception('no bolometric data')
        
        from scipy.interpolate import interp1d
        
        imin,imax = self.tvec.searchsorted((tmin, tmax))
        imax += 1
        tvec = self.tvec[imin:imax]
        ntim = len(tvec)

        data_all     = np.empty((ntim, self.nl), dtype=np.single)
        data_err_all = np.empty((ntim, self.nl), dtype=np.single)

        for i, det in enumerate(self.activ_cam):
            data_tvec = blb.gettimebase('tim'+det)
            ## [osam] debugging  #BUG
            # try:
            #     nbeg,nend = data_tvec.searchsorted([tmin,tmax])
            # except:
            #     import pdb
            #     pdb.set_trace()
            nbeg,nend = data_tvec.searchsorted([tmin,tmax])
            data_tvec = data_tvec[nbeg:nend]
            data = blb.getobject('pow'+det, cal=True)[nbeg: nend, :]
            data = data[:, : self.nchan[det]][:, self.active_los[det][: self.nchan[det]]]

            #dowsample to 0.001s resolution
            nt = nend-nbeg
            reduct = int(np.ceil(self.dt/(tmax-tmin)*nt))
            nt = (nt//reduct)*reduct
            data = np.reshape(data[:nt,:],(nt//reduct,reduct,-1))
            dataerr = np.std(data, axis=1)
            data   = np.mean(data, axis=1)
            
            data_tvec = np.reshape(data_tvec[:nt],(nt//reduct,reduct))
            data_tvec = data_tvec.mean(1)

            if len(data_tvec) > 1:
                data_tvec[[0,-1]] = tmin-1,tmax+1
                data = interp1d(data_tvec,data,fill_value =0,bounds_error=True,
                            copy=False,axis=0,assume_sorted=True)(tvec)
                dataerr = interp1d(data_tvec,dataerr,fill_value=0,bounds_error=True,
                            copy=False,axis=0,assume_sorted=True)(tvec)

            dataerr[np.isnan(data)] = np.infty
            data[np.isnan(data)] = 0
            data_all[:,  self.cam_ind[det]] = data
            data_err_all[:, self.cam_ind[det]] = dataerr

        def apply_correction(cam,channels,corr):
            if cam in self.cam_ind:
                ind = np.in1d(self.channels[cam], channels)
                data_all    [:, self.cam_ind[cam][ind]] *= corr
                data_err_all[:, self.cam_ind[cam][ind]] *= corr

        data_err_all+= np.nanmean(data_all,1)[:,None]*self.min_error+self.sigma*data_all
        #BUG adhoc corrections of the foil bolometers

        if self.shot > 31591: #BUG find the accurate upper boundary!!
            apply_correction( 'FHC', np.r_[12:17], 1.3)
            if self.shot < 32000:
                apply_correction( 'FVC', np.r_[16:49], 0.58)

        if self.shot > 30135:
            apply_correction( 'FVC',  16, 2.4)
            apply_correction( 'FVC',  np.r_[0:48], 0.87)
            apply_correction( 'FHC',  np.r_[36:48], 0.76)
            apply_correction( 'FHC',  33, 0.91)
            apply_correction( 'FHC',  34, 0.95)
            apply_correction( 'FHC',  np.r_[20:24], 1.2)
            ##apply_correction( 'FHC',  np.r_[0:12],0.7)
            apply_correction( 'FHC',  np.r_[0:48], 0.87)
            apply_correction( 'FHC',  32, 1.1)
            apply_correction( 'FHC',  [22,25,30], 1.05)
    
        elif self.shot > 28523:
            apply_correction( 'FVC',  np.r_[16:48], 1/1.75)
            apply_correction( 'FHC',  np.r_[36:48], 1/1.1)
            apply_correction( 'FHC',  [33,36], 1/1.1)

        elif self.shot > 27352:
            apply_correction( 'FHC',  33, 1/1.2)
            apply_correction( 'FHC',  34, 1/1.1)

        apply_correction( 'FHS', np.r_[0:48],1.1)

        return  tvec, data_all, data_err_all

 
    def load_geom(self,path):
        
        if self.shot < 26000:  #just guess!!
            self.geometry_version = 4
        elif self.shot < 29100:  #just guess!!
            self.geometry_version = 3
        elif self.shot < 30162 : 
            self.geometry_version = 4
        elif self.shot < 31776 : 
            self.geometry_version = 2
        else: 
            self.geometry_version = 1

        for i,det in enumerate(self.activ_cam):

            R1 = self.R_start[det]
            Z1 = self.z_start[det]
            R2 = self.R_end[det] 
            Z2 = self.z_end[det]

            #move the begining of LOS to the center of the fan
            I = np.ones_like(R1)
            if det == 'FLX': R1, R2, Z1, Z2 = 1.899*I, R1, -0.945*I, Z1
            if det == 'FDO': R1, R2, Z1, Z2 = 1.473*I, R2, -1.173*I, Z2
            if det == 'FDI': R1, R2, Z1, Z2 = 1.313*I, np.minimum(R1,R2), -1.148*I, np.maximum(Z1, Z2)
       
            m_alpha = abs(abs(np.arctan2(Z2-Z1,R2-R1))-np.pi/2)
            alphas = np.arctan2(Z2-Z1,R2-R1)
            alphas = np.unwrap(alphas)
            m_alpha = np.mean(m_alpha)

            # xfile = open(self.geometry_path+'/detector_%s_x.txt'%det,'w')
            # yfile = open(self.geometry_path+'/detector_%s_y.txt'%det,'w')

            alpha = np.arctan2(Z2-Z1,R2-R1) #to same jako alphas? 
            
            theta = np.ones_like(alpha)*np.median(abs(np.diff(alphas)))/2 #exactly nonoverlaping LOS 
            theta/= 4  #actual LOS are narrower...
                        
            if det == 'FHC':
                theta[:9] = np.median(abs(np.diff(alphas))[:9])/2
                theta[9:32] = np.median(abs(np.diff(alphas))[9:32])/2
                theta[32:] = np.median(abs(np.diff(alphas))[32:])/2

            L = np.hypot(R2-R1, Z2-Z1)

            verts = []

            if m_alpha < np.pi/4:       
                Lr = L*abs(np.sin(alpha)/np.sin(np.pi-theta-alpha))
                Ll = L*abs(np.sin(np.pi-alpha)/np.sin(alpha-theta))
                R21 = R1+Ll*np.cos(alpha-theta)
                R22 = R1+Lr*np.cos(alpha+theta)
                
                np.savetxt(self.geometry_path+'/detector_%s_x.txt'%det, np.c_[R21,R22,R1], fmt='%5.4f')
                np.savetxt(self.geometry_path+'/detector_%s_y.txt'%det, np.c_[Z2,Z1], fmt='%5.4f')
                print(f"BOLO detectors data saved {det} in {self.geometry_path} [1]")
                for r1,r21,z1,r22,z2 in zip(R1,R21,Z1,R22,Z2):
                    verts.append([[r1,z1],[r21,z2 ],[r22,z2]])

            else:
                Z21 = (Z1-(R2-R1)*np.tan(np.pi-abs(alpha)-theta)*np.sign(alpha))
                Z22 = (Z1-(R2-R1)*np.tan(np.pi-abs(alpha)+theta)*np.sign(alpha))
 
                np.savetxt(self.geometry_path+'/detector_%s_x.txt'%det, np.c_[R2,R1], fmt='%5.4f')
                np.savetxt(self.geometry_path+'/detector_%s_y.txt'%det, np.c_[Z21,Z22,Z1], fmt='%5.4f')  
                print(f"BOLO detectors data saved {det} in {self.geometry_path} [2]")
                for z1,z21,r1,z22,r2 in zip(Z1,Z21,R1,Z22,R2):
                    verts.append([[r1,z1],[r2,z21],[r2,z22]])
