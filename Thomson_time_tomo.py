import os
from shared_modules import read_config
from prepare_data import *
from main import tomography
from make_graphs import * # for plotting
import numpy as np
import pickle
# import GUI_solution
import GUI
import VTA_sfLoad_osam as VTA

def find_nearest_idx(array, value):
    "find nearest idx in array for the value"
    idx = (np.abs(array - value)).argmin()
    return idx

### Configure the paths and load the inputs dictionary 
cfg_file = "tomography"
program_path = os.path.abspath(__file__)
program_path = program_path[:program_path.rfind('/')]+'/'        
# local_path = os.path.expanduser('~/tomography/')
local_path = "/shares/users/work/osam/tomography/"
inputs = read_config(local_path+cfg_file+".cfg")
 
inputs['program_path']= program_path
inputs['local_path']  = local_path
# inputs['output_path'] = os.path.expanduser(os.path.expandvars(inputs['output_path']))+'/'
inputs['output_path'] = '/shares/users/work/osam/tomography/Output'
# inputs['tmp_folder']  = os.path.expanduser(os.path.expandvars(inputs['tmp_folder']))+'/'
inputs['tmp_folder']  = '/shares/users/work/osam/tomography/tmp'

config.wrong_dets_pref = inputs['wrong_dets']
if 'usecache' in inputs:
    config.useCache = inputs['usecache']


### Manual data entry
Shot = int(30529)
# inputs['shot'] = int(30583)
inputs['shot'] = Shot
# inputs['tmin'] = 3.2008
# inputs['tmax'] = 3.2015  
# inputs['tmin'] = 3.2001
# inputs['tmax'] = 3.2001  
inputs['tok_index'] = 10 # 9: ASDEX SXR, 10: ASDEX SXRfast
inputs['plot_all'] = False # separated plot for each time step
inputs['gnuplot'] = False # plot with gnuplot (faster than in matplotlib)
inputs['plot_contours'] = True # check contour plot


VT=VTA.VTA()
VT.Load(Shotnumber = Shot, EQU="EQH")
tB_Th = 1.5
tE_Th = 8.0
idx_B_Th = find_nearest_idx(VT.time_c, tB_Th)
idx_E_Th = find_nearest_idx(VT.time_c, tE_Th)
time_Th = VT.time_c[idx_B_Th:idx_E_Th]
NNt_Th = len(time_Th)
emissSXR = np.zeros([inputs['ny'], inputs['nx'], NNt_Th])
timeSXR = np.zeros([NNt_Th])

# for i_t in range(2):
for i_t in range(NNt_Th):
    print("############")
    print(f"Calculating tomography {i_t+1}/{NNt_Th}")
    print("############")
    inputs['tmin'] = time_Th[i_t]
    inputs['tmax'] = time_Th[i_t]+5.0e-7
    ### Load and prepare the tokamak data (geometry, SXR configs)
    tok = loaddata(inputs)
    tok.prepare_tokamak()

    ### Initiate tomography calculations
    inputs, tokamak, progress,output = tomography(inputs,tok)

    ### Plotting 
    make_graphs((inputs, tokamak, progress, output), plot_svd=False)

    fileload = "Emissivity_%0.3f-%0.3f_rec_%g.npz"%(inputs['tmin'], inputs['tmax'], Shot)
    Emissivity = np.load(inputs['tmp_folder']+"/"+fileload, allow_pickle=True)
    gres, tvec, rvec, zvec, inputs, gres_norm, BdMat = Emissivity['gres'], Emissivity['tvec'], Emissivity['rvec'], Emissivity['zvec'], Emissivity['inputs'].all(), Emissivity['gres_norm'],  Emissivity['BdMat']
    emissSXR[:,:,i_t] = gres[:,:,0]*gres_norm[0] # [W*m-3]
    timeSXR[i_t] = tvec[0]

#saving data
if (1==1):
    path_to_save = "/shares/users/work/osam/output_my_programs/" 
    name_to_save = "Thoms_time_tomo_%g.npz"%(Shot)
    np.savez_compressed(path_to_save+name_to_save, emissSXR=emissSXR, timeSXR=timeSXR,\
        rvec=rvec, zvec=zvec, shot=Shot, time_Th=time_Th)
    print(f"Data is succesfully saved in {path_to_save} as {name_to_save}.")
