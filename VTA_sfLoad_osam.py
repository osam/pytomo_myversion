"""
import modules.VTA_sfLoad_osam as VTA
use as
VT=VTA.VTA()
VT.Load(Shotnumber = Shot, EQU="EQH")
"""
import numpy as np
import aug_sfutils as sf

class VTA:
    def __init__(self,  Experiment = 'AUGD', Diagnostic = 'VTA',
                Shotnumber = None, EQU =None,
                tBegin=-1.0, tEnd=12.0):
        
        if (Shotnumber != None):
            self.Load(Shotnumber)
            
    def Load(self, Shotnumber, Experiment='AUGD',
             Diagnostic='VTA', Edition=0, EQU=None,
             tBegin=-1.0, tEnd=12.0):
        self.Shotnumber = Shotnumber
        self.Experiment = Experiment
        self.Diagnostic = Diagnostic
        self.Edition = Edition
        self.tBegin = tBegin
        self.tEnd = tEnd
        self.eqm = EQU
        print("Reading %s.." % (Diagnostic))
        sfload = sf.SFREAD(Diagnostic, Shotnumber,
            experiment=Experiment, edition=Edition)
        
        # self.Te_c = sfload.getobject("Te_c", cal=True, tbeg=tBegin, tend=tEnd)

        self.Te_c = sfload.getobject("Te_c")
        self.Te_e = sfload.getobject("Te_e")
        self.Ne_c = sfload.getobject("Ne_c")
        self.Ne_e = sfload.getobject("Ne_e")
        
        self.SigNe_c = sfload.getobject("SigNe_c")
        self.SigNe_e = sfload.getobject("SigNe_e")
        self.SigTe_c = sfload.getobject("SigTe_c")
        self.SigTe_e = sfload.getobject("SigTe_e")

        # !!! TODO time should be cut manually
        r_core = sfload.getobject("R_core")
        z_core = sfload.getobject("Z_core")
        r_edge = sfload.getobject("R_edge")
        z_edge = sfload.getobject("Z_edge")
        # self.time_c = sfload.getobject("TIM_CORE", tbeg=tBegin, tend=tEnd)
        # self.time_e = sfload.getobject("TIM_EDGE", tbeg=tBegin, tend=tEnd)
        self.time_c = sfload.getobject("TIM_CORE")
        self.time_e = sfload.getobject("TIM_EDGE")

        # restructure core and edge R,z
        self.R_core = np.zeros([len(self.time_c), len(z_core)])
        self.Z_core = np.zeros_like(self.R_core)
        self.R_edge = np.zeros([len(self.time_e), len(z_edge)])
        self.Z_edge = np.zeros_like(self.R_edge)
        for i_t in range(len(self.time_c)):
            self.R_core[i_t, :] = r_core[i_t]*np.ones(len(z_core))
            self.Z_core[i_t, :] = z_core

        for i_t in range(len(self.time_e)):
            self.R_edge[i_t, :] = r_edge[i_t]*np.ones(len(z_edge))
            self.Z_edge[i_t, :] = z_edge

    def LoadAllRhop(self):
        "load rhop from tBegin until tEnd"
        if self.eqm is not None:
            self.rhop_core = sf.rz2rho(
                self.eqm,
                self.R_core,
                self.Z_core,
                t_in=self.time_c,
                coord_out="rho_pol",
                extrapolate=True,
            )
            self.rhop_edge = sf.rz2rho(
                self.eqm,
                self.R_edge,
                self.Z_edge,
                t_in=self.time_e,
                coord_out="rho_pol",
                extrapolate=True,
            )
            print("### Thomson: rhop_core and rhop_edge are loaded ###")
        else:
            print("!!! please load the equilibrium and parse to the load module !!!")
            print("!!! rhops are not loaded !!!")
