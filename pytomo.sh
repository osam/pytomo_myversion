#!/bin/bash -f
source /etc/profile.d/modules.sh

module purge 
module load mkl
module load intel/2023.1.0.x
module load gnuplot
 
#export LD_LIBRARY_PATH=${MKL_HOME}/lib/intel64_lin

#load mencoder nd ffmpeg
#export PATH=${PATH}/afs/@cell/common/soft/visualization/mencoder/ffmpeg/2.7/amd64_sles11/bin/
#export PATH=${PATH}/afs/@cell/common/soft/visualization/mencoder/svn-2012-11-15/amd64_sles11/bin/
#export PATH=${HOME}/.local/lib/python3.10/site-packages:${PATH}
#export PYTHONPATH=${HOME/.local/lib/python3.10/site-packages}:${PATH}
 
#export LD_LIBRARY_PATH=${MKL_HOME}/lib/intel64_lin:/afs/ipp-garching.mpg.de/home/t/todstrci/SuiteSparse/lib/
#export C_INCLUDE_PATH=${LD_LIBRARY_PATH}:/afs/ipp-garching.mpg.de/home/t/todstrci/SuiteSparse/lib/
#export LD_LIBRARY_PATH=${MKL_HOME}/lib/intel64_lin:/tok/u/osam/SuiteSparse-4.5.3/lib
#export C_INCLUDE_PATH=${LD_LIBRARY_PATH}:/tok/u/osam/SuiteSparse-4.5.3/lib
export LD_LIBRARY_PATH=${MKL_HOME}/lib/intel64_lin:/shares/departments/AUG/users/osam/SuiteSparse-7.1.0/lib
export C_INCLUDE_PATH=${LD_LIBRARY_PATH}:/shares/departments/AUG/users/osam/SuiteSparse-7.1.0/lib
#export PYTHONPATH=/shares/software/aug-dv/moduledata/aug_sfutils/0.8.0:${PYTHONPATH}

export MKL_NUM_THREADS=1
module load anaconda/3/2023.03
module load pymysql # requirements for aug_sfutils
#module load aug_sfutils/0.9.0 # 0.9.1 will not work (different syntex)
module load aug_sfutils # 0.9.1 will not work (different syntex)


rootdir=`dirname $0`                       # may be relative path
export PYTOMO=`cd $rootdir && pwd`  # ensure absolute path
cd $PYTOMO

python3 ./pytomo.py  $@
