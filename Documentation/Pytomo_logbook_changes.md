# Pytomo: changes im myversion

## problem with loading fastSXR (08.09.2022):

```python
========== RECONSTRUCTION STARTED ==========
phantom_generator Emiss
loading 0% diag:SXA FO:
10:05:13 | aug_sfutils.sfread | INFO: Fetching SF /afs/ipp-garching.mpg.de/home/a/augd/shots/3057/SX/SXA/30579
10:05:14 | aug_sfutils.sfread | ERROR: Status of SF object I_043 is 1
some error detectedint() argument must be a string, a bytes-like object or a number, not 'NoneType'
Reconstruction failed
Traceback (most recent call last):
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/GUI.py", line 1675, in run
 output_list = tomography(self.setting, self.tokamak, self.progress)
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/main.py", line 78, in tomography
 inputs['data_smooth'], inputs['data_undersampling'], inputs['error_scale'], inputs['prune_dets'], False)
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/tokamak.py", line 672, in prepare_data
 tvec,data,error = self.get_data(tmin=tmin-dt,tmax=tmax+dt)
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/geometry/ASDEX/ASDEX.py", line 364, in get_data
 tvec, data, error = loader.get_data(tmin,tmax)
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/geometry/ASDEX/SXR/get_sxr.py", line 219, in get_data
 return self.get_data_fast(tmin,tmax)
 File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_myversion/geometry/ASDEX/SXR/get_sxr.py", line 512, in get_data_fast
 data[:, i] = sfo.getobject(sig, cal=False, nbeg=imin, nend=imax+1)
 File "/mpcdf/soft/SLE_15/packages/x86_64/anaconda/3/2019.03/lib/python3.7/site-packages/numpy/ma/core.py", line 3323, in __setitem__
 _data[indx] = dval
 TypeError: int() argument must be a string, a bytes-like object or a number, not 'NoneType'
 ^C^C./pytomo.sh: line 29: 26170 Aborted (core dumped) python ./pytomo.py $@
```

the problem is in

./geometry/ASDEX/SXR/get_sxr.py

in the function: get_data_fast(tmin,tmax)

### problem description

```python
# some fast SXR signals are not written
# so the command:
sfo.getobject(sig,cal=False, nbeg=0, nend=1)
# gives an empty array with "NoneType"
# that is why the error above appear
```

### Proposed solution:

check the fast SXR signals during the loading process and exclude those that are empty:

```python
# file "get_sxr.py", line 204
if fast_data:
    # recheck the channels
    self.SXR_diods = OrderedDict()
    for sfile in unique(shotfiles):
        self.SXR_diods[sfile] = []

     for sfile, sig in zip(shotfiles, signals):
        #remove T camera and null signals
        if (sig[0] != 'T') and (sfile != 'OOO'):
            sfo = sf.SFREAD(sfile, self.shot)
            test_fast_sxr_data = sfo.getobject(sig,cal=False, nbeg=0, nend=1)
            #remove T camera and NoneType signals
            if (test_fast_sxr_data != None):
                self.SXR_diods[sfile].append(sig)

    self.SXR_diods.pop('OOO')  #null detector
```

Solution worked

## Problem with length of tvec and data_tvec (08.09.2022)

```python
File "/afs/ipp-garching.mpg.de/home/o/osam/git_repositories/pytomo_dummy_loader/geometry/ASDEX/SXR/get_sxr.py", line 580, in get_data_fast
    raise Exception('Wrong length of tvec!! %d  %d'%(len(data_tvec), len(tvec)))
Exception: Wrong length of tvec!! 
len(data_tvec)=1405  len(tvec)=1406
```

### Problem description

sometimes data_tvec and tvec values have different lengths (just one element shorter or longer). This problem appears due to the different scheme of data_tvec and tvec calculation:

**tvec**

```python
# file "get_sxr.py", line 204
if fast_data:
    sxa = sf.SFREAD('SXA', self.shot)
    if not sxa.status:
        raise Exception('SXA shotfile is not avalible')
    self.tvec = sxa('Time')
else:
    ssx = sf.SFREAD('SSX', self.shot, exp=self.experiment, ed=self.ed)
    if not ssx.status:
        raise Exception('SSX shotfile is not avalible')
    self.tvec = ssx('time')

# line 458
imin_,imax_ = self.tvec.searchsorted((tmin, tmax))
tvec = self.tvec[imin_:imax_+1]
```

**data_tvec**

```python
# file "get_sxr.py", line 523
sfo = sf.SFREAD(DAS, self.shot)
tb = sfo('Time')
tbeg = tb[0]
tend = tb[-1]
tlen = len(tb)
imin,imax = (r_[tmin, tmax] - tbeg)/(tend - tbeg)*(tlen - 1)
imin,imax = int(ceil(imin)), int(ceil(min(imax, tlen)))
data_tvec = tb[imin: imax+1]
```

### Proposed solution:

calculate tvec and data_tvec in the same way

```python
# file "get_sxr.py", line 515
tb = sfo('Time')
# [osam]: make data_tvec and self.tvec to be calculated in the same way
imin,imax = tb.searchsorted((tmin, tmax))
data_tvec = tb[imin: imax+1]

# file "get_sxr.py", line 458
imin_,imax_ = self.tvec.searchsorted((tmin, tmax))
tvec = self.tvec[imin_:imax_+1] 
```

## Problem with calibration of fastSXR (06.02.2023):

**Fast SXR data loads succesfully, but the calibration is corrupted.**

Manual calibration of fast SXR is done in the following way:

"get_sxr.py" in "./geometry/ASDEX/SXR/" loads the uncalibrated data with the "aug_sfutils" AUG library, then this uncalibrated data is manual calibrated in the same file "get_sxr.py".

There is a possibility to load calibrated data directly with the "aug_sfutils", for example

```python
import aug_sfutils as sf
for diag_name, signals in SXR_diods.items():
    sfo = sf.SFREAD(diag_name, shot, experiment="AUGD", edition=int(0))
    for j, sig_name in enumerate(signals):
        dict_time["%s"%sig_name] = np.asarray(sfo.gettimebase(sig_name,tbeg = t_min, tend = t_max))
        dict_sig["%s"%sig_name] = np.asarray(sfo.getobject(sig_name, cal=True, tbeg = t_min, tend = t_max))
```

```python
sfo.getobject(sig_name, cal=True, tbeg = t_min, tend = t_max)
# here "cal=True", which orders to make the fastSXR calibration 
# within the "aug_sfutils" library
```

I have compared the slow and fast calibrated SXR data read by "get_sxr.py" and by "aug_sfutils". For a couple of shots, the data looks identical. This means that the problem with calibrated fastSXR data somewhere else in the "pytomo" code.

In the pytomo GUI, the fast SXR data is loaded through line:

```python
inputs, tokamak, progress, output = tomography(inputs,tok)
```

the loaded fast SXR data is saved in the "output" as "output['data']" and "output['data_all']". And this stage the calibration of the fast SXR is false, so the problem is in the "tomography(inputs,tok)" command.

"tomography" is the class from the "main.py" file.

Inside this "tomography" class the fastSXR data is loade through line:

```python
data_all, error_all, tvec, dets, Tmat_all, normData = tok.prepare_data(inputs['tmin'], inputs['tmax'], inputs['data_smooth'], inputs['data_undersampling'], inputs['error_scale'], inputs['prune_dets'], False)
```

where "tok" is the tokamak class from the "tokamak.py" file.

tokamak class has a method "prepare_data":

```python
def prepare_data(self, tmin, tmax, data_smooth, data_undersampling, error_scale, 
                 prune_dets = 1,  detsCutOff = True, allowed_dets = None):
        """
        Main function that prepare data.

        *   Crops data to slected range ``tmin``, ``tmax``
        *   Use under sampling to decrease number of reconstructed/plotted timeslices.
        *   Use moving average to smooth the data.
        *   Remove data that belongs to ``wrong_dets``
        *   Remove NaN detectors
        *   Renormalize data form sensors according to tokamak.calb


        :param class self: ``Class`` of  tokamak with all important data.
        :param double tmin, tmax: Time range reconstruction
        :param int data_smooth: Number bins used for regularization (moving average)
        :param int data_undersampling:    Use every `n`-th snapshot
        :param double error_scale:  Increase size of expected errobars
        :param bool detsCutOff:  Remove wrong detectors from data
        :var array data:  Final array of data
        :var array tvec:  Final array of time vecor
        :var array dets:  Final array of dets
        :var int normTmat:    Norm of geometry matrix
        :var array normData:  Maximum of data in each timeslice
        """
```

this "prepare_data" method loads fastSXR data through "get_sxr.py" library and then prepares the loaded data for the tomography (smooth, remove wrong_dets, remove NaN, renormalization). 

The "prepare_data" method also has a calibration algorithm, if self.calb!=1:

```python
        if not all(self.calb==1):
            if len(self.calb) == self.Ndets:
                for i in range(self.Ndets):
                    data[dets_index[i], :] *= mean(self.calb[i])   # if case of different time vector
                    error[dets_index[i], :] *= mean(self.calb[i]) 
            elif len(self.calb) == self.nl:
                data*= self.calb[:,None]
                error*= self.calb[:,None]
```

The if sentence "if not all(self.calb==1):" works incorrectly within the "pytomo" framework, since even when the data is calibrated correctly through "get_sxr.py" library the "prepare_date" initiates the second calibration nevertheless.

### Proposed solution:

There are two manual calibration of the fast SXR data: 1) in "get_sxr.py", 2) in "tokamak.py" within the "prepare_data" method.

Proposed solution is remove the second calibration routine in the "tokamak.py flie" (lines 757-765).

```python
        """ # [osam]: double calibration (in get_sxr.py and here)
        if not all(self.calb==1):
            if len(self.calb) == self.Ndets:
                for i in range(self.Ndets):
                    data[dets_index[i], :] *= mean(self.calb[i])   # if case of different time vector
                    error[dets_index[i], :] *= mean(self.calb[i]) 
            elif len(self.calb) == self.nl:
                data*= self.calb[:,None]
                error*= self.calb[:,None]
        """
```

### Side note on the fast SXR loading routine

There is a possibility to rewrite "get_sxr.py" to remove manual calibration there. Instead using the standard AUG calibration routine for "aug_sfutils" library. However, there calibration with "aug_sfutils" is much slower (at least 10x times) than the manually implemented by Tomas in "get_sxr.py" file. That is why, I decided to keep the "get_sxr.py" routine unchanged.



## Problem with exclusion SXR channels

### Problem description

The were two problems:

1) In channel slices, the end point was not included. It is according python-syntax, but contradicts human logic. e.g in slice "1:4", only channels "1,2,3" are included but not the "4".

2) In shifting all channels in GUI to "+1"  in comparison to channel indexing in the backend calculation. E.g. the channels "1,2,3" are read as "0,1,2". It is done probably to make it more "human-readable". However, the code with this shift becomes less readable , it becomes easier to make a mistake in this shift.



### Proposed solution

To make channels numbering in the GUI and in the backend the same and include the last channel in slicing. e.g channels "1,2,5:7" will be read as "1,2,5,6,7".

The changes are made inside the "data_preprocessing.py" file:

```python
# data_preprocessing.py line 2088
    def ReadWrongDets(self,init=False):
        dets = str(self.Edit_wrongDetectors.text())
        if len(dets.strip()) > 0:
            try:
                # [osam]: include the end point in slices
                # dets_list = eval('r_['+dets+']')
                # wrong_dets_pref = array(int_(dets_list)-1, ndmin=1)
                mylist = dets.split(",")
                wrong_dets_pref = array([]).astype(int)
                for el in mylist:
                    if ":" in el:
                        tempchs = eval('r_['+el+']')
                        tempchs = array(int_(tempchs), ndmin=1)
                        tempchs = append(tempchs, tempchs[-1]+int(1))
                    else:
                        tempchs = eval('r_['+el+']')
                        tempchs = array(int_(tempchs), ndmin=1)
                    wrong_dets_pref = append(wrong_dets_pref, tempchs)
            except:
                wrong_dets_pref = []
                QMessageBox.warning(self,"Input problem",
                    "Wrong detectors are in bad format, use ...,10,11,13:15,...",QMessageBox.Ok)
                raise
        elif init:     # do not remove on init
            wrong_dets_pref = config.wrong_dets_pref
        else:
            wrong_dets_pref = []
        return  wrong_dets_pref

# data_preprocessing.py line 2111

    def SetWrongDets(self, wrong):
        
        wrong = unique(wrong)
        print("SetWrongDets, wrong:")
        print(wrong)

        wrong_dets_str = ''
        i = 0
        while i < len(wrong):
            # [osam]: remove the +1 shift in the beginning, numbering as in background python dets dictionary
            # wrong_dets_str = wrong_dets_str+str(wrong[i]+1)
            wrong_dets_str = wrong_dets_str+str(wrong[i])
            if i+1 < len(wrong) and wrong[i+1] == wrong[i]+1:
                while i+1 < len(wrong) and wrong[i+1] == wrong[i]+1:
                    i += 1
                # [osam]: change the +2 shift to match numbering as in background python dets dictionary, plus including the end point in slices
                # wrong_dets_str = wrong_dets_str+':'+str(wrong[i]+2)
                wrong_dets_str = wrong_dets_str+':'+str(wrong[i])
            i += 1
            wrong_dets_str = wrong_dets_str+','
        wrong_dets_str = wrong_dets_str[:-1]
            
        self.Edit_wrongDetectors.setText(wrong_dets_str)

```
